<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/19/2017
 * Time: 12:33 PM
 */

namespace OGC\GML;


class DirectPosition extends Position
{

    const TAG_POS = 'pos';

    private $coordinate;


    public function __construct(Coordinate $coordinate)
    {

        parent::__construct();
        $this->coordinate = $coordinate;

    }


    public function getCoordinate(): Coordinate{
        return $this->coordinate;
    }


    public function toXML(bool $prettify = false): string
    {

        $attributes = parent::generateAttributes();

        $xml = sprintf('%s%s%s',
            $this->generateOpenTag(self::TAG_POS, $attributes),
            $this->coordinate->toXML($prettify),
            $this->generateCloseTag(self::TAG_POS));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}