<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/19/2017
 * Time: 12:20 PM
 */

namespace OGC\GML;


use OGC\XML;

class AbstractGeometricObject extends AbstractObject
{

    const NAMESPACE_PREFIX = 'gml';

    const ATTR_SRS_DIMENSION = 'srsDimension';
    const ATTR_SRS_NAME = 'srsName';
    const ATTR_UOM_LABELS = 'uomLabels';
    const ATTR_AXIS_LABELS = 'axisLabels';

    protected $srsName;
    protected $srsDimension;
    protected $axisLabels;
    protected $uomLabels;


    protected function __construct()
    {

        parent::__construct();

    }


    public function toXML(bool $prettify = false): string
    {
        return '';
    }


    public function setSrsName(string $srsName): self{
        $this->srsName = $srsName;
        return $this;
    }


    public function setSrsDimension(int $dimensions): self{
        $this->srsDimension = $dimensions;
        return $this;
    }


    public function setAxisLabels(string $axisLabels): self{
        $this->axisLabels = $axisLabels;
        return $this;
    }

    public function setUomLabels(string $uomLables): self{
        $this->uomLabels = $uomLables;
        return $this;
    }


    protected function generateAttributes(): array{

        $attributes = [];

        if($this->srsName)
            $attributes[self::ATTR_SRS_NAME] = $this->srsName;
        if($this->srsDimension)
            $attributes[self::ATTR_SRS_DIMENSION] = $this->srsDimension;
        if($this->uomLabels)
            $attributes[self::ATTR_UOM_LABELS] = $this->uomLabels;
        if($this->axisLabels)
            $attributes[self::ATTR_AXIS_LABELS] = $this->axisLabels;

        return $attributes;

    }


}