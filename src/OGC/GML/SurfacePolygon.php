<?php
/**
 * A Polygon is a special surface that is defined by a single surface patch (see D.3.6). The boundary of this patch is
 * coplanar and the polygon uses planar interpolation in its interior. The elements exterior and interior describe the
 * surface boundary of the polygon.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/21/2017
 * Time: 8:52 AM
 */

namespace OGC\GML;


class SurfacePolygon extends SurfacePrimitive
{

    const TAG_POLYGON = 'polygon';

    private $exterior;
    private $interiors = [];


    public function __construct(?SurfaceExterior $exterior = null, SurfaceInterior ...$interiors)
    {

        parent::__construct();
        $this->exterior = $exterior;
        $this->interiors = $interiors;

    }


    protected function generateChildXml($prettify): string {

        $xml = parent::generateChildXml($prettify);

        //Exterior
        if($this->exterior)
            $xml .= sprintf(($prettify) ? "\n%s" : '%s', $this->exterior->toXML($prettify));

        //Interiors
        foreach($this->interiors as $interior)
            $xml .= sprintf(($prettify) ? "\n%s" : '%s', $interior->toXML($prettify));

        return $xml;

    }


    public function addInteriors(SurfaceInterior ...$interiors): self {

        $this->interiors = array_merge($this->interiors, $interiors);
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        $xml = sprintf(($prettify) ? "%s\t%s\n%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_POLYGON, parent::generateAttributes()),
            preg_replace("/\n/", "\n\t", $this->generateChildXml($prettify)),
            $this->generateCloseTag(self::TAG_POLYGON));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}