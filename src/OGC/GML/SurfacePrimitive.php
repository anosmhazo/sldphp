<?php
/**
 * An abstraction of a surface to support different levels of complexity
 *
 * User: Anos Kuda Mhazo
 * Date: 12/21/2017
 * Time: 8:33 AM
 */

namespace OGC\GML;


abstract class SurfacePrimitive extends GeometricPrimitive
{

}