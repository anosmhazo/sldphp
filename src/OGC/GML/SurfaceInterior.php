<?php
/**
 * A boundary of a surface consists of a number of rings. The "interior" rings separate the surface/surface patch from
 * the area enclosed by the rings.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/21/2017
 * Time: 8:57 AM
 */

namespace OGC\GML;


class SurfaceInterior extends SurfacePrimitive
{

    const TAG_INTERIOR = 'interior';

    private $linearRing;

    public function __construct(RingLinear $linearRing)
    {
        parent::__construct();
        $this->linearRing = $linearRing;

    }


    public function toXML(bool $prettify = false): string
    {

        //Generate attributes
        $attributes = $this->generateAttributes();

        //Generate envelope
        $xml = sprintf(($prettify) ? "%s\n\t%s\n%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_INTERIOR, $attributes),
            preg_replace("/\n/", "\n\t", $this->linearRing->toXML($prettify)),
            $this->generateCloseTag(self::TAG_INTERIOR));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}