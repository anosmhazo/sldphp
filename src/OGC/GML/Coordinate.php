<?php
/**
 * Represents 1D, 2D, 3D ...nD set of coordinates
 *
 * User: Anos Kuda Mhazo
 * Date: 12/19/2017
 * Time: 12:08 PM
 */

namespace OGC\GML;


class Coordinate extends AbstractGeometricObject
{

    const DELIMITER_DEFAULT = '';

    private $delimiter = self::DELIMITER_DEFAULT;
    private $dimensions;
    private $ordinates = [];


    public function  __construct(array $ordinates, int $dimensions)
    {

        //Init parent
        parent::__construct();

        if(count($ordinates) > 0 && $dimensions > 0 && count($ordinates) == $dimensions){

            $this->dimensions = $dimensions;
            $this->ordinates = $ordinates;

        }else{

            throw new \Exception("Invalid coordinate");

        }

    }


    public function getDimensions(): int{
        return $this->dimensions;
    }


    public function setDelimiter(string $delimiter): self{

        $this->delimiter = $delimiter;
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        return join($this->delimiter.' ', $this->ordinates);

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}