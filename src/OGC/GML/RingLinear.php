<?php
/**
 * A gml:LinearRing is defined by four or more coordinate tuples, with linear interpolation between them; the first
 * and last coordinates shall be coincident.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/21/2017
 * Time: 10:03 AM
 */

namespace OGC\GML;


class RingLinear extends RingPrimitive
{

    const TAG_RING = 'linearRing';

    private $coordinates = [];

    public function __construct(Coordinate ...$coordinates)
    {
        parent::__construct();
        $this->coordinates = $coordinates;
    }


    public function addCoordinates(Coordinate ...$coordinates): self{

        $this->coordinates = array_merge($this->coordinates, $coordinates);
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        //Use pos list
        $posList = new DirectPositionList(...$this->coordinates);

        $xml = sprintf(($prettify) ? "%s\n\t%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_RING, parent::generateAttributes()),
            preg_replace("/\n/", "\n\t", $posList->toXML($prettify)),
            preg_replace("/\n/", "\n\t", parent::generateChildXml($prettify)),
            $this->generateCloseTag(self::TAG_RING));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}