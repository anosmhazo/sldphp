<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/20/2017
 * Time: 10:33 AM
 */

namespace OGC\GML;


abstract class GeometricPrimitive extends AbstractGeometricObject
{

    const ATTR_ID = 'gml:id';

    protected $id;
    private $names = [];
    private $description;
    private $identifier;


    public function setId(?string $id): self{

        $this->id = $id;
        return $this;

    }


    protected function generateAttributes(): array{

        $attributes = parent::generateAttributes();

        if($this->id)
            $attributes[self::ATTR_ID] = $this->id;

        return $attributes;

    }



    protected function generateChildXml($prettify): string{

        $xml = '';

        //Names
        foreach($this->names as $name)
            $xml .= sprintf(($prettify) ? "\n%s" : '%s', $name->toXML($prettify));

        //Description
        if($this->description)
            $xml .= sprintf(($prettify) ? "\n%s" : '%s', $this->description->toXML($prettify));

        //Identifier
        if($this->identifier)
            $xml .= sprintf(($prettify) ? "\n%s" : '%s', $this->identifier->toXML($prettify));

        return $xml;

    }


    public function addNames(GMLName ...$names): self{

        $this->names = array_merge($this->names, $names);
        return $this;

    }


    public function setDescription(?GMLDescription $description): self{

        $this->description = $description;
        return $this;

    }


    public function setIdentifier(?GMLIdentifier $identifier): self{

        $this->identifier = $identifier;
        return $this;

    }

}