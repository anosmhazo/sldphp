<?php
/**
 * Defined as a single coordinate tuple.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/20/2017
 * Time: 11:16 AM
 */

namespace OGC\GML;


class CurveLineString extends CurvePrimitive
{

    const TAG_LINESTRING = 'lineString';

    private $coordinates;


    public function __construct(Coordinate ...$coordinates)
    {

        parent::__construct();
        $this->coordinates = $coordinates;

    }


    public function addCoordinates(Coordinate ...$coordinates): self{

        $this->coordinates = array_merge($this->coordinates, $coordinates);
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        //Use pos list
        $posList = new DirectPositionList(...$this->coordinates);

        $xml = sprintf(($prettify) ? "%s\n\t%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_LINESTRING, parent::generateAttributes()),
            preg_replace("/\n/", "\n\t", $posList->toXML($prettify)),
            preg_replace("/\n/", "\n\t", parent::generateChildXml($prettify)),
            $this->generateCloseTag(self::TAG_LINESTRING));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}