<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/20/2017
 * Time: 11:36 AM
 */

namespace OGC\GML;


class GMLDescription extends AbstractObject
{

    const TAG_DESCRIPTION = 'description';
    const TAG_DESCRIPTION_REFERENCE = 'descriptionReference';

    const ATTR_TYPE = 'xlink:type';
    const ATTR_HREF = 'xlink:href';
    const ATTR_ROLE = 'xlink:role';
    const ATTR_ARCROLE = 'xlink:arcrole';
    const ATTR_TITLE = 'xlink:title';
    const ATTR_SHOW = 'xlink:show';
    const ATTR_ACTUATE = 'xlink:actuate';
    const ATTR_NIL_REASON = 'nilReason';
    const ATTR_OWNS = 'owns';
    const ATTR_REMOTE_SCHEMA = 'gml:remoteSchema';

    private $description;
    private $owns;
    private $type;
    private $href;
    private $role;
    private $arcrole;
    private $title;
    private $show;
    private $actuate;
    private $nilReason;
    private $remoteSchema;

    private $isReference = false;

    public function __construct(?string $description)
    {
        parent::__construct();
        $this->description = $description;

    }


    private function generateAttributes(): array{

        $attributes = [];

        if($this->type)
            $attributes[self::ATTR_TYPE] = $this->type;
        if($this->href)
            $attributes[self::ATTR_HREF] = $this->href;
        if($this->role)
            $attributes[self::ATTR_ROLE] = $this->role;
        if($this->arcrole)
            $attributes[self::ATTR_ARCROLE] = $this->arcrole;
        if($this->title)
            $attributes[self::ATTR_TITLE] = $this->title;
        if($this->show)
            $attributes[self::ATTR_SHOW] = $this->show;
        if($this->actuate)
            $attributes[self::ATTR_ACTUATE] = $this->actuate;
        if($this->nilReason)
            $attributes[self::ATTR_NIL_REASON] = $this->nilReason;
        if($this->remoteSchema)
            $attributes[self::ATTR_REMOTE_SCHEMA] = $this->remoteSchema;

        return $attributes;

    }


    public function isReference(bool $reference = true): self{

        $this->isReference = $reference;
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        $xml = '';

        if(!$this->isReference){

            $xml = sprintf('%s%s%s',
                $this->generateOpenTag(self::TAG_DESCRIPTION, $this->generateAttributes()),
                $this->description,
                $this->generateCloseTag(self::TAG_DESCRIPTION));

        }else{

            $xml = $this->generateOpenSelfClosingTag(self::TAG_DESCRIPTION, $this->generateAttributes());

        }

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}