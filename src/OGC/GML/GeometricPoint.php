<?php
/**
 * Defined as a single coordinate tuple.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/20/2017
 * Time: 11:16 AM
 */

namespace OGC\GML;


class GeometricPoint extends GeometricPrimitive
{

    const TAG_POINT = 'point';

    private $position;

    public function __construct(DirectPosition $position)
    {

        parent::__construct();
        $this->position = $position;

    }


    public function toXML(bool $prettify = false): string
    {

        $xml = sprintf(($prettify) ? "%s\n\t%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_POINT, parent::generateAttributes()),
            preg_replace("/\n/", "\n\t", $this->position->toXML($prettify)),
            preg_replace("/\n/", "\n\t", parent::generateChildXml($prettify)),
            $this->generateCloseTag(self::TAG_POINT));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}