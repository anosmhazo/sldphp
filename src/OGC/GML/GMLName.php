<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/20/2017
 * Time: 11:36 AM
 */

namespace OGC\GML;


class GMLName extends AbstractObject
{

    const TAG_NAME = 'name';
    const ATTR_CODE_SPACE = 'codeSpace';

    private $name;
    private $codeSpace;


    public function __construct(string $name, ?string $codeSpace = null)
    {
        parent::__construct();

        $this->name = $name;
        $this->codeSpace = $codeSpace;

    }


    public function toXML(bool $prettify = false): string
    {

        $attributes = [];

        if($this->codeSpace)
            $attributes[self::ATTR_CODE_SPACE] = $this->codeSpace;

        $xml = sprintf('%s%s%s',
            $this->generateOpenTag(self::TAG_NAME, $attributes),
            $this->name,
            $this->generateCloseTag(self::TAG_NAME));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}