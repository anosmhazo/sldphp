<?php
/**
 * The root element for an SLD is <StyledLayerDescriptor>. It contains a sequence of Layers defining the styled map
 * content.
 * User: Anos Kuda Mhazo
 * Date: 1/5/2018
 * Time: 11:48 AM
 */

namespace OGC\SLD;


use OGC\XML;

class StyledLayerDescriptor extends XML
{

    const TAG_DESCRIPTOR = 'StyledLayerDescriptor';

    const ATTR_VERSION = 'version';
    const ATTR_SCHEMA_LOCATION = 'xsi:schemaLocation';
    const ATTR_XMLNS = 'xmlns';
    const ATTR_OGC = 'xmlns:ogc';
    const ATTR_XLINK = 'xmlns:xlink';
    const ATTR_XSI = 'xmlns:xsi';

    private $layers = [];
    private $version = '1.0.0';
    private $includeXmlVersion = false;


    public function __construct(Layer... $layers)
    {
        $this->layers = $layers;
    }


    public function addLayers(Layer... $layers): self {

        $this->layers = array_merge($this->layers, $layers);
        return $this;

    }


    public function removeLayers(int $index, int $len = -1): self{

        $len = ($len < 0) ? count($this->layers) : $len;
        array_splice($this->layers, $index, $len);
        return $this;

    }


    public function includeXmlVersion(bool $include = true): self {
        $this->includeXmlVersion = $include;
        return $this;
    }



    private function layersToXml(bool $prettify){

        //Reduce array to XML string
        return array_reduce($this->layers, function($carry, $layer) use ($prettify){

            $carry .= strlen($carry) && $prettify ? "\n" : "";
            $carry .= $layer->toXML($prettify);
            return $carry;

        }, '');

    }


    protected function generateAttributes(): array{

        $attributes = [
            self::ATTR_VERSION => $this->version,
            self::ATTR_SCHEMA_LOCATION => 'http://www.opengis.net/sld StyledLayerDescriptor.xsd',
            self::ATTR_XMLNS => 'http://www.opengis.net/sld',
            self::ATTR_OGC => 'http://www.opengis.net/ogc',
            self::ATTR_XLINK => 'http://www.w3.org/1999/xlink',
            self::ATTR_XSI => 'http://www.w3.org/2001/XMLSchema-instance'
        ];

        return $attributes;

    }


    public function toXML(bool $prettify = false): string
    {

        $xml_xml = '';
        $layers_xml = '';
        $newline = $prettify ? "\n" : "";

        //Generate XML version xml
        if($this->includeXmlVersion)
            $xml_xml = $this->generateXMLVersionXML().$newline;

        if($this->layers)
            $layers_xml = $newline.$this->layersToXml($prettify);

        $xml = sprintf(($prettify) ? "%s%s%s\n%s" : '%s%s%s%s',
            $xml_xml,
            $this->generateOpenTag(self::TAG_DESCRIPTOR, $this->generateAttributes()),
            preg_replace("/\n/", "\n\t", $layers_xml),
            $this->generateCloseTag(self::TAG_DESCRIPTOR));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}