<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/18/2017
 * Time: 3:30 PM
 */

namespace OGC\SLD\FE;


class DistanceSpatialOperator extends SpatialOperator
{

    const OPERATOR_BEYOND = 'Beyond';
    const OPERATOR_DWITHIN = 'DWithin';
    const TAG_DISTANCE = 'Distance';

    public $expression1;
    public $expression2;
    public $operatorType;
    public $distance;
    public $uom;


    public function __construct(string $operator, Expression $expression1, Expression $expression2, float $distance,
                                string $uom = self::UOM_METRE)
    {
        $this->operatorType = $operator;
        $this->distance = $distance;
        $this->uom = $uom;
        $this->expression1 = $expression1;
        $this->expression2 = $expression2;

    }


    public function toXML(bool $prettify = false): string
    {

        $distance_xml = sprintf(
            '%s%s%s',
            $this->generateOpenTag(self::TAG_DISTANCE, [self::ATTR_UOM => $this->uom]),
            $this->distance,
            $this->generateCloseTag(self::TAG_DISTANCE)
        );

        $xml = sprintf(($prettify) ? "%s\t%s%s\n\t%s\n%s" : '%s%s%s%s%s',
            $this->generateOpenTag($this->operatorType),
            preg_replace("/\n/", "\n\t", $this->expression1->toXML($prettify)),
            preg_replace("/\n/", "\n\t", $this->expression2->toXML($prettify)),
            preg_replace("/\n/", "\n\t", $distance_xml),
            $this->generateCloseTag($this->operatorType));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}