<?php
/**
 * This OGC Filter Encoding Standard defines a standard set of comparison operators (=,<,>,>=,<=,<>): equal to, less than,
 * greater than, less than or equal to, greater than or equal to and not equal to.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/14/2017
 * Time: 11:23 AM
 */

namespace OGC\SLD\FE;


class BinaryComparisonOperator extends ComparisonOperator
{

    //Comparison names
    const PROPERTY_IS_EQUAL_TO = 'PropertyIsEqualTo';
    const PROPERTY_IS_NOT_EQUAL_TO = 'PropertyIsNotEqualTo';
    const PROPERTY_IS_LESS_THAN = 'PropertyIsLessThan';
    const PROPERTY_IS_GREATER_THAN = 'PropertyIsGreaterThan';
    const PROPERTY_IS_LESS_THAN_OR_EQUAL_TO = 'PropertyIsLessThanOrEqualTo';
    const PROPERTY_IS_GREATER_THAN_OR_EQUAL_TO = 'PropertyIsGreaterThanOrEqualTo';

    //Match names
    const MATCH_ACTION_ANY = 'any';
    const MATCH_ACTION_ALL = 'all';
    const MATCH_ACTION_ONE = 'one';

    //Atrribute names
    const ATTR_MATCH_ACTION = 'matchAction';
    const ATTR_MATCH_CASE = 'matchCase';


    private $operatorType;
    private $expression;
    private $matchCase;
    private $matchAction;


    public function __construct(string $operatorType, Expression $expression, ?bool $matchCase = null, ?string $matchAction = null)
    {

        $this->operatorType = $operatorType;
        $this->expression = $expression;
        $this->matchCase = $matchCase;
        $this->matchAction = $matchAction;

    }


    public function toXML(bool $prettify = false): string
    {

        //Check if expression is valid
        if(!$this->expression instanceof Expression)
            throw new \Exception('Invalid expression.');

        //Generate attributes
        $attributes = [];
        if($this->matchAction !== null)
            $attributes[self::ATTR_MATCH_ACTION] = $this->matchAction;
        if($this->matchCase !== null)
            $attributes[self::ATTR_MATCH_CASE] = var_export($this->matchCase, true);

        //Generate xml
        $xml = sprintf(($prettify) ? "%s%s\n%s" : '%s%s%s',
            $this->generateOpenTag($this->operatorType, $attributes),
            preg_replace("/\n/", "\n\t", $this->expression->toXML($prettify)),
            $this->generateCloseTag($this->operatorType));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}