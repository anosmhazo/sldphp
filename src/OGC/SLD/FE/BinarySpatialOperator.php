<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/18/2017
 * Time: 3:30 PM
 */

namespace OGC\SLD\FE;


class BinarySpatialOperator extends SpatialOperator
{

    const OPERATOR_BBOX= 'BBOX';
    const OPERATOR_EQUALS = 'Equals';
    const OPERATOR_DISJOINT = 'Disjoint';
    const OPERATOR_INTERSECTS = 'Intersects';
    const OPERATOR_TOUCHES = 'Touches';
    const OPERATOR_CROSSES = 'Crosses';
    const OPERATOR_WITHIN = 'Within';
    const OPERATOR_CONTAINS = 'Contains';
    const OPERATOR_OVERLAPS = 'Overlaps';

    private $operatorType;
    private $expression1;
    private $expression2;


    public function __construct(string $operator, Expression $expression1, Expression $expression2)
    {
        $this->operatorType = $operator;
        $this->expression1 = $expression1;
        $this->expression2 = $expression2;
    }


    public function toXML(bool $prettify = false): string
    {

        $xml = sprintf(($prettify) ? "%s\t%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag($this->operatorType),
            preg_replace("/\n/", "\n\t", $this->expression1->toXML($prettify)),
            preg_replace("/\n/", "\n\t", $this->expression2->toXML($prettify)),
            $this->generateCloseTag($this->operatorType));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}