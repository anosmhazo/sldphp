<?php
/**
 * This OGC Filter Encoding Standard defines NilComparisonOperator as an operator that tests the content of the specified
 * property and evaluates if nil.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/14/2017
 * Time: 11:23 AM
 */

namespace OGC\SLD\FE;


class NilComparisonOperator extends ComparisonOperator
{

    //Comparison names
    const TAG_NIL = 'PropertyIsNil';

    private $expression;
    private $nilReason;


    public function __construct(Expression $expression, ?string $nilReason = '')
    {

        $this->expression = $expression;
        $this->nilReason = $nilReason;

    }


    public function toXML(bool $prettify = false): string
    {

        $xml = sprintf(($prettify) ? "%s%s\n%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_NIL),
            preg_replace("/\n/", "\n\t", $this->expression->toXML($prettify)),
            $this->generateCloseTag(self::TAG_NIL));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}