<?php
/**
 * Generates XML encoding of logical AND, OR operators according to OGC Filter Encoding standard 09-026r1
 * as defined on pages 28 to 29.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/18/2017
 * Time: 10:13 AM
 */

namespace OGC\SLD\FE;


class BinaryLogicalOperator extends LogicalOperator
{

    const OPERATION_AND = 'And';
    const OPERATION_OR = 'Or';

    private $operation;
    private $leftoperator;
    private $rightoperator;
    private $moreoperators = [];


    public function  __construct(string $operation, FilterOperator $left, FilterOperator $right)
    {
        $this->operation = $operation;
        $this->leftoperator = $left;
        $this->rightoperator = $right;
    }


    public function addOperators(FilterOperator ...$operators): self{

        $this->moreoperators = array_merge($this->moreoperators, $operators);
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        $xml = sprintf(($prettify) ? "%s\n\t%s\n\t%s\n\t%s\n%s" : '%s%s%s%s%s',
            $this->generateOpenTag($this->operation),
            preg_replace("/\n/", "\n\t", $this->leftoperator->toXML($prettify)),
            preg_replace("/\n/", "\n\t", $this->rightoperator->toXML($prettify)),
            $this->moreToXML($prettify),
            $this->generateCloseTag($this->operation));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


    private function moreToXML($prettify): string{

        $xml = '';
        foreach($this->moreoperators as $operator){
            $xml .= preg_replace("/\n/", "\n\t", $operator->toXML($prettify));
        }

        return $xml;

    }

}