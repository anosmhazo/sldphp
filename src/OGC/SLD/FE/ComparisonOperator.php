<?php
/**
 * A comparison operator as defined in OGC Filter Encoding Standard is used to form expressions that evaluate the
 * mathematical comparison between two arguments. If the arguments satisfy the comparison then the expression evaluates
 * to true. Otherwise the expression evaluates to false. Comparison operators comprise BinaryComparisonOperator,
 * LikeComparisonOperator, BetweenComparisonOperator, NullComparisonOperator and NilComparisonOperator.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/14/2017
 * Time: 11:11 AM
 */

namespace OGC\SLD\FE;


abstract class ComparisonOperator extends FilterOperator
{
}