<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/18/2017
 * Time: 3:28 PM
 */

namespace OGC\SLD\FE;


abstract class SpatialOperator extends FilterOperator
{

    const UOM_METRE = 'm';

    //Attributes
    const ATTR_UOM = 'unit';

}