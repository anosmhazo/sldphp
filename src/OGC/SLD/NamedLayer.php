<?php
/**
 * Encapsulates a reference to a named layer in the server catalog.
 *
 * User: Anos Kuda Mhazo
 * Date: 1/5/2018
 * Time: 11:53 AM
 */

namespace OGC\SLD;


use OGC\SLD\SE\RuleDescription;

class NamedLayer extends Layer
{

    const TAG_NAMED_STYLE = 'NamedLayer';


    /**
     * @var NamedStyle[]
     */
    public $namedStyles = [];


    /**
     * @var UserStyle[]
     */
    public $styles = [];


    public function __construct(?string $name = null, ?RuleDescription $description = null)
    {
        parent::__construct();
        $this->setName($name)->setDescription($description);
    }


    public function addStyles(Style ...$styles): self {

        $this->styles = array_merge($this->styles, $styles);
        return $this;

    }


    public function removeStyles(int $index, int $len = -1): self{

        $len = ($len < 0) ? count($this->styles) : $len;
        array_splice($this->styles, $index, $len);
        return $this;

    }


    private function stylesToXml(bool $prettify){

        //Reduce array to XML string
        return array_reduce($this->styles, function($carry, $style) use ($prettify){

            $carry .= strlen($carry) && $prettify ? "\n" : "";
            $carry .= $style->toXML($prettify);
            return $carry;

        }, '');

    }


    public function toXML(bool $prettify = false): string
    {

        $styles = '';
        $newline = $prettify ? "\n" : "";
        $parent_xml = $this->generateParentTags($prettify);

        if($this->styles)
            $styles = $newline.$this->stylesToXml($prettify);

        $xml = sprintf(($prettify) ? "%s%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_NAMED_STYLE),
            preg_replace("/\n/", "\n\t", $parent_xml),
            preg_replace("/\n/", "\n\t", $styles),
            $this->generateCloseTag(self::TAG_NAMED_STYLE));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}