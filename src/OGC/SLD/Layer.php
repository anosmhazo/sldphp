<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/5/2018
 * Time: 11:53 AM
 */

namespace OGC\SLD;


use OGC\SLD\SE\RuleDescription;

abstract class Layer extends SLD
{

    const TAG_NAME = 'Name';


    /**
     * @var string
     */
    protected $name;


    /**
     * @var RuleDescription
     */
    protected $description;


    public function setName(?string $name): self{
        $this->name = $name;
        return $this;
    }


    public function getName(): ?string {
        return $this->name;
    }


    public function setDescription(?RuleDescription $description): self{
        $this->description = $description;
        return $this;
    }


    protected function generateParentTags(bool $prettify): string {

        $xml = '';
        $newline = $prettify ? "\n" : "";

        if($this->name)
            $xml .= $newline.sprintf('%s%s%s',
                    $this->generateOpenTag(self::TAG_NAME),
                    $this->name,
                    $this->generateCloseTag(self::TAG_NAME));

        if($this->description)
            $xml .= $newline.$this->description->toXML($prettify);


        return $xml;

    }

}