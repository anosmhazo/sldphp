<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/5/2018
 * Time: 11:57 AM
 */

namespace OGC\SLD;


use OGC\SLD\SE\RuleDescription;

class NamedStyle extends Style
{

    const TAG_NAMED_STYLE = 'NamedStyle';

    public function __construct(?string $name = null, ?RuleDescription $description = null)
    {
        $this->setName($name)->setDescription($description);
    }


    public function toXML(bool $prettify = false): string
    {

        $newline = $prettify ? "\n" : "";
        $parent_xml = $this->generateParentTags($prettify);
        $parent_xml = (strlen($parent_xml) > 0) ? $newline.$parent_xml : $parent_xml;

        $xml = sprintf(($prettify) ? "%s%s\n%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_NAMED_STYLE),
            preg_replace("/\n/", "\n\t", $parent_xml),
            $this->generateCloseTag(self::TAG_NAMED_STYLE));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}