<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/4/2018
 * Time: 8:53 AM
 */

namespace OGC\SLD\SE;


class GraphicStroke extends SE
{

    const TAG_GRAPHIC_STROKE = 'GraphicStroke';

    /**
     * @var Graphic
     */
    private $graphic;


    public function __construct(Graphic $graphic)
    {

        parent::__construct();
        $this->graphic = $graphic;

        //Disable namespace by default
        $this->applyNamespace(false);

    }


    public function toXML(bool $prettify = false): string
    {

        return sprintf($prettify ? "%s\n\t%s%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_GRAPHIC_STROKE),
            preg_replace("/\n/", "\n\t", $this->graphic->toXML($prettify)),
            $this->generateCloseTag(self::TAG_GRAPHIC_STROKE));

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}