<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/11/2018
 * Time: 11:02 AM
 */

namespace OGC\SLD\SE;


class WellKnownName extends SE
{

    const TAG_WELL_KNOWN_NAME = 'WellKnownName';


    /* SLD MANDATORY NAMES */

    const SLD_CIRCLE = 'circle';        //A circle
    const SLD_TRIANGLE = 'triangle';    //A triangle pointing up
    const SLD_SQUARE = 'square';        //A square
    const SLD_STAR = 'star';            //A five-pointed star
    const SLD_CROSS = 'cross';          //A square cross with space around (not suitable for hatch fills)
    const SLD_X = 'x';                  //A square X with space around (not suitable for hatch fills)

    const GEOSERVER_SHAPE_VERTLINE = 'shape://vertline';        //A vertical line (suitable for hatch fills or to make railroad symbols)
    const GEOSERVER_SHAPE_HORLINE = 'shape://horline';          //A horizontal line (suitable for hatch fills)
    const GEOSERVER_SHAPE_SLASH = 'shape://slash';              //A diagonal line leaning forwards like the “slash” keyboard symbol (suitable for diagonal hatches)
    const GEOSERVER_SHAPE_BACKSLASH = 'shape://backslash';      //Same as shape://slash, but oriented in the opposite direction
    const GEOSERVER_SHAPE_DOT = 'shape://dot';                  //A very small circle with space around
    const GEOSERVER_SHAPE_PLUS = 'shape://plus';                //A + symbol, without space around (suitable for cross-hatch fills)
    const GEOSERVER_SHAPE_TIMES = 'shape://times';              //A “X” symbol, without space around (suitable for cross-hatch fills)
    const GEOSERVER_SHAPE_OARROW = 'shape://oarrow';            //An open arrow symbol (triangle without one side, suitable for placing arrows at the end of lines)
    const GEOSERVER_SHAPE_CARROW = 'shape://carrow';            //A closed arrow symbol (closed triangle, suitable for placing arrows at the end of lines)

    /**
     * @var string
     */
    private $name;


    public function __construct(string $name = self::SLD_CIRCLE)
    {

        parent::__construct();
        $this->name = $name;

        //Disable namespace by default
        $this->applyNamespace(false);

    }


    public function toXML(bool $prettify = false): string
    {

        return sprintf('%s%s%s',
            $this->generateOpenTag(self::TAG_WELL_KNOWN_NAME),
            $this->name,
            $this->generateCloseTag(self::TAG_WELL_KNOWN_NAME));

    }


    public function __toString()
    {

        return $this->toXML(true);

    }


}