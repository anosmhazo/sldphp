<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/4/2018
 * Time: 8:53 AM
 */

namespace OGC\SLD\SE;


class Graphic extends SE
{

    const TAG_GRAPHIC = 'Graphic';

    private $externalGraphic;
    private $mark;
    private $opacity;
    private $size;
    public $rotation;


    public function __construct(?ExternalGraphic $externalGraphic = null, ?Mark $mark = null,
                                ?Opacity $opacity = null, ?Size $size = null, ?Rotation $rotation = null)
    {

        parent::__construct();

        $this->externalGraphic = $externalGraphic;
        $this->mark = $mark;
        $this->opacity = $opacity;
        $this->size = $size;
        $this->rotation = $rotation;

    }


    public function setExternalGraphic(?ExternalGraphic $externalGraphic): self {

        $this->externalGraphic = $externalGraphic;
        return $this;

    }


    public function setMark(?Mark $mark): self {

        $this->mark = $mark;
        return $this;

    }


    public function setOpacity(?Opacity $opacity): self {

        $this->opacity = $opacity;
        return $this;

    }


    public function setSize(?Size $size): self {

        $this->size = $size;
        return $this;

    }


    public function setRotation(?Rotation $rotation): self {

        $this->rotation = $rotation;
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        $externalGraphic_xml = '';
        $mark_xml = '';
        $opacity_xml = '';
        $size_xml = '';
        $rotation_xml = '';

        $newline = $prettify ? "\n" : "";

        //External Graphic
        if($this->externalGraphic)
            $externalGraphic_xml = $newline.$this->externalGraphic->toXML();

        //Mark
        if($this->mark)
            $mark_xml = $newline.$this->mark->toXML($prettify);

        //Opacity
        if($this->opacity)
            $opacity_xml = $newline.$this->opacity->toXML($prettify);

        //Size
        if($this->size)
            $size_xml = $newline.$this->size->toXML($prettify);

        //Rotation
        if($this->rotation)
            $rotation_xml = $newline.$this->rotation->toXML($prettify);


        $xml = sprintf(($prettify) ? "%s%s%s%s%s%s\n%s" : '%s%s%s%s%s%s%s',
            $this->generateOpenTag(self::TAG_GRAPHIC),
            preg_replace("/\n/", "\n\t", $externalGraphic_xml),
            preg_replace("/\n/", "\n\t", $mark_xml),
            preg_replace("/\n/", "\n\t", $opacity_xml),
            preg_replace("/\n/", "\n\t", $size_xml),
            preg_replace("/\n/", "\n\t", $rotation_xml),
            $this->generateCloseTag(self::TAG_GRAPHIC));

        return $xml;

    }


    public function __toString()
    {
       return $this->toXML(true);
    }


}