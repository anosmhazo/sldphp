<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/12/2018
 * Time: 2:07 PM
 */

namespace OGC\SLD\SE;


class LabelPlacement extends SE
{

    const TAG_LABEL_PLACEMENT = 'LabelPlacement';

    private $pointPlacement;
    private $linePlacement;

    public function __construct(?PointPlacement $pointPlacement = null, ?LinePlacement $linePlacement = null)
    {
        parent::__construct();

        $this->pointPlacement = $pointPlacement;
        $this->linePlacement = $linePlacement;

    }


    public function setPointPlacement(?PointPlacement $pointPlacement = null): self {

        $this->pointPlacement = $pointPlacement;
        return $this;

    }


    public function setLinePlacement(?LinePlacement $linePlacement = null): self {

        $this->linePlacement = $linePlacement;
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        $point_xml = '';
        $line_xml = '';

        $newline = $prettify ? "\n" : "";

        //Point placement
        if($this->pointPlacement)
            $point_xml = $newline.$this->pointPlacement->toXML($prettify);

        //Line placement
        if($this->linePlacement)
            $line_xml = $newline.$this->linePlacement->toXML($prettify);

        return sprintf(($prettify) ? "%s%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_LABEL_PLACEMENT),
            preg_replace("/\n/", "\n\t", $point_xml),
            preg_replace("/\n/", "\n\t", $line_xml),
            $this->generateCloseTag(self::TAG_LABEL_PLACEMENT));

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}