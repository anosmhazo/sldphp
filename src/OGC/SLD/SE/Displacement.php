<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/12/2018
 * Time: 2:10 PM
 */

namespace OGC\SLD\SE;


class Displacement extends SE
{

    const TAG_DISPLACEMENT = 'Displacement';
    const TAG_DISPLACEMENT_X = 'DisplacementX';
    const TAG_DISPLACEMENT_Y = 'DisplacementY';

    private $x;
    private $y;


    public function __construct(float $x, float $y)
    {

        parent::__construct();

        $this->x = $x;
        $this->y = $y;

    }


    public function toXML(bool $prettify = false): string
    {

        $newline = $prettify ? "\n" : "";

        $displacementX = $newline.sprintf('%s%s%s',
                $this->generateOpenTag(self::TAG_DISPLACEMENT_X),
                $this->x,
                $this->generateCloseTag(self::TAG_DISPLACEMENT_X));

        $displacementY = $newline.sprintf('%s%s%s',
                $this->generateOpenTag(self::TAG_DISPLACEMENT_Y),
                $this->y,
                $this->generateCloseTag(self::TAG_DISPLACEMENT_Y));

        return sprintf(($prettify) ? "%s%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_DISPLACEMENT),
            preg_replace("/\n/", "\n\t", $displacementX),
            preg_replace("/\n/", "\n\t", $displacementY),
            $this->generateCloseTag(self::TAG_DISPLACEMENT));

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}