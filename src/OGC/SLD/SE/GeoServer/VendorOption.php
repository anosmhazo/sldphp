<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/12/2018
 * Time: 5:20 PM
 */

namespace OGC\SLD\SE\GeoServer;


use OGC\SLD\SE\SE;

class VendorOption extends SE
{

    const TAG = 'VendorOption';

    const ATTR_NAME = 'name';

    //LINES

    /**
     * The group option allows displaying a single label for multiple features in a logical group.
     */
    const NAME_GROUP = 'group';

    /**
     * The labelAllGroup option can be used in conjunction with the group option. It causes all of the disjoint paths
     * in a line group to be labeled, not just the longest one.
     */
    const NAME_LABEL_ALL_GROUP = 'labelAllGroup';

    /**
     * By default GeoServer will not render labels “on top of each other”. By using the spaceAround option you can
     * either allow labels to overlap, or add extra space around labels. The value supplied for the option is a positive
     * or negative size, in pixels.
     */
    const NAME_SPACE_AROUND = 'spaceAround';

    /**
     * The followLine option forces a label to follow the curve of the line. You don’t need to use followLine for
     * straight lines. GeoServer will automatically follow the orientation of the line. However in this case followLine
     * can be used to ensure the text isn’t rendered if longer than the line.
     */
    const NAME_FOLLOW_LINE = 'followLine';

    /**
     * The maxDisplacement option controls the displacement of the label along a line, around a point and inside a polygon.
     */
    const NAME_MAX_DISPLACEMENT = 'maxDisplacement';

    /**
     * The repeat option determines how often GeoServer displays labels along a line. Normally GeoServer labels each
     * line only once, regardless of length.
     */
    const NAME_REPEAT = 'repeat';

    /**
     * When used in conjunction with followLine, the maxAngleDelta option sets the maximum angle, in degrees, between
     * two subsequent characters in a curved label. Large angles create either visually disconnected words or overlapping
     * characters. It is advised not to use angles larger than 30.
     */
    const NAME_MAX_ANGLE_DELTA = 'maxAngleDelta';

    /**
     * The autoWrap option wraps labels when they exceed the given width (in pixels). The size should be wide enough to
     * accommodate the longest word, otherwise single words will be split over multiple lines.
     */
    const NAME_AUTO_WRAP = 'autoWrap';

    /**
     * The renderer tries to draw labels along lines so that the text is upright, for maximum legibility. This means a
     * label may not follow the line orientation, but instead may be rotated 180° to display the text the right way up.
     * In some cases altering the orientation of the label is not desired; for example, if the label is a directional
     * arrow showing the orientation of the line.
     */
    const NAME_FORCE_LEFT_TO_RIGHT = 'forceLeftToRight';

    /**
     * GeoServer will remove labels if they are a particularly bad fit for the geometry they are labeling.
     */
    const NAME_GOODNESS_OF_IT = 'goodnessOfFit';

    /**
     * GeoServer normally tries to place labels horizontally within a polygon, and gives up if the label position is
     * busy or if the label does not fit enough in the polygon. This option allows GeoServer to try alternate rotations
     * for the labels.
     */
    const NAME_POLYGON_ALIGN = 'polygonAlign';

    /**
     * When a <Graphic> is specified for a label by default it is displayed at its native size and aspect ratio. The
     * graphic-resize option instructs the renderer to magnify or stretch the graphic to fully contain the text of the
     * label. If this option is used the graphic-margin option may also be specified.
     */
    const NAME_GRAPHIC_RESIZE = 'graphic-resize';

    /**
     * The graphic-margin options specifies a margin (in pixels) to use around the label text when the  graphic-resize
     * option is specified.
     */
    const NAME_GRAPHIC_MARGIN = 'graphic-margin';

    /**
     * The partials options instructs the renderer to render labels that cross the map extent, which are normally not
     * painted since there is no guarantee that a map put on the side of the current one (tiled rendering) will contain
     * the other half of the label. By enabling “partials” the style editor takes responsibility for the other half being there (maybe because the label points have been placed by hand and are assured not to conflict with each other, at all zoom levels).
     */
    const NAME_PARTIALS = 'partials';

    /**
     * The underlineText option instruct the renderer to underline labels. The underline will work like a typical word
     * processor text underline. The thickness and position of the underline will be defined by the font and color will
     * be the same as the text. Spaces will also be underlined.
     */
    const NAME_UNDERLINE_TEXT= 'underlineText';

    /**
     * The strikethroughText option instruct the renderer to strikethrough labels. The strikethrough will work like a
     * typical word processor text strikethrough. The thickness and position of the line will be defined by the font and
     * color will be the same as the text. Spaces will also be stroken.
     */
    const NAME_STRIKETHROUGH_TEXT = 'strikethroughText';

    /**
     * The charSpacing option controls the amount of space between characters, a positive value increases it, a negative
     * value shrinks it (and will eventually make characters overlap). The value is specified in pixels.
     */
    const NAME_CHAR_SPACING = 'charSpacing';

    /**
     * The wordSpacing option controls the amount of space between words, for this option only positive values (or zero)
     * are accepted. The value is specified in pixels.
     */
    const NAME_WORD_SPACING = 'wordSpacing';

    private $name;
    private $value;

    protected function generateAttributes(): array{

        $attributes = [];

        if($this->name)
            $attributes[self::ATTR_NAME] = $this->name;

        return $attributes;

    }


    public function __construct(string $name, $value = null)
    {

        parent::__construct();

        $this->name = $name;
        $this->value = $value;

    }


    public function toXML(bool $prettify = false): string
    {
        return sprintf('%s%s%s',
            $this->generateOpenTag(self::TAG, $this->generateAttributes()),
            $this->value,
            $this->generateCloseTag(self::TAG));
    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}