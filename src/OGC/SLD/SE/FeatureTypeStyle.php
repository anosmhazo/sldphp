<?php
/**
 * Feature Type Style is the root element of a Symbology Encoding. It defines the styling that is to be applied to a
 * single feature type. The XML for Feature Type Style is defined on page 5 of OGC standard 05-077r4 Symbology Encoding.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/11/2017
 * Time: 2:36 PM
 */

namespace OGC\SLD\SE;


class FeatureTypeStyle extends SE
{

    const TAG_FEATURE_TYPE_STYLE = 'FeatureTypeStyle';
    const TAG_NAME = 'Name';
    const TAG_FEATURE_TYPE_NAME = 'FeatureTypeName';
    const TAG_SEMANTIC_TYPE_IDENTIFIER = 'SemanticTypeIdentifier';

    const ATTR_VERSION = 'version';

    /**
     * @var string
     */
    private $name;

    /**
     * @var RuleDescription
     */
    private $description;

    /**
     * @var string
     */
    private $featureTypeName;

    /**
     * @var string
     */
    private $semanticTypeIdentifier;

    /**
     * @var Rule[]
     */
    private $rules = [];

    /**
     * @var string
     */
    private $version;


    /**
     * FeatureTypeStyle constructor.
     * @param string $name
     * @param null|RuleDescription $description
     * @param Rule[] $rules
     */
    public function __construct(?string $name = null, ?RuleDescription $description = null, Rule ...$rules){

        parent::__construct();

        $this->setName($name)->setDescription($description);
        $this->rules = $rules;

    }

    /**
     * Get SE name. Not explicitly used. Reserved for future use.
     * @return string
     */
    public function getName(): string{
        return $this->name;
    }


    /**
     * Get human readable information about SE.
     * @return RuleDescription
     */
    public function getDescription(): RuleDescription{
        return $this->description;
    }


    /**
     * Get specific feature type the style is for.
     * @return string
     */
    public function getFeatureTypeName(): string{
        return $this->$this->featureTypeName;
    }


    /**
     * Get semantic identifier of type for use in several feature types
     * @return string
     */
    public function getSemanticTypeIdentifier(): string{
        return $this->semanticTypeIdentifier;
    }


    /**
     * Get feature type rules
     * @return array
     */
    public function getRules(): array{
        return $this->rules;
    }


    /**
     * Get the SE version number
     * @return string
     */
    public function getVersion(): string{

        return $this->version;

    }


    /**
     * Sets name of feature type style
     * @param string $name
     * @return FeatureTypeStyle
     */
    public function setName(?string $name): self{

        $this->name = $name;
        return $this;

    }


    /**
     * Sets description of feature type style
     * @param null|RuleDescription $description
     * @return FeatureTypeStyle
     */
    public function setDescription(?RuleDescription $description) : self{

        $this->description = $description;
        return $this;

    }


    /**
     * Sets feature type name
     * @param null|string $name
     * @return FeatureTypeStyle
     */
    public function setFeatureTypeName(?string $name) : self{

        $this->featureTypeName = $name;
        return $this;

    }


    public function setSemanticTypeIndentifier(?string $identifier) : self{

        $this->semanticTypeIdentifier = $identifier;
        return $this;

    }


    /**
     * Adds rule to feature type style and returns new total number of rules
     * @param Rule[] $rules
     * @return FeatureTypeStyle
     */
    public function addRules(Rule ...$rules): self{

        $this->rules = array_merge($this->rules, $rules);
        return $this;

    }


    /**
     * Removes rules from specified index to specified (index + length)
     * and returns total number of rules remaining
     * @param int $index
     * @param int $len
     * @return FeatureTypeStyle
     */
    public function removeRules(int $index, int $len = -1): self{

        $len = ($len < 0) ? count($this->rules) : $len;
        array_splice($this->rules, $index, $len);
        return $this;

    }


    private function rulesToXml(bool $prettify){

        //Reduce array to XML string
        return array_reduce($this->rules, function($carry, $rule) use ($prettify){

            $carry .= strlen($carry) && $prettify ? "\n" : "";
            $carry .= $rule->toXML($prettify);
            return $carry;

        }, '');

    }


    public function toXML(bool $prettify = false): string
    {

        $description_xml = '';
        $name_xml = '';
        $rules_xml = '';
        $featureTypeName_xml = '';
        $semanticTypeIdentifier_xml = '';
        $newline = $prettify ? "\n" : "";

        if($this->name)
            $name_xml = $newline.sprintf('%s%s%s',
                    $this->generateOpenTag(self::TAG_NAME),
                    $this->name,
                    $this->generateCloseTag(self::TAG_NAME));
        if($this->description)
            $description_xml = $newline.$this->description->toXML($prettify);
        if($this->rules)
            $rules_xml = $newline.$this->rulesToXml($prettify);

        if($this->featureTypeName)
            $featureTypeName_xml = $newline.sprintf('%s%s%s',
                    $this->generateOpenTag(self::TAG_FEATURE_TYPE_NAME),
                    $this->featureTypeName,
                    $this->generateCloseTag(self::TAG_FEATURE_TYPE_NAME));

        if($this->semanticTypeIdentifier)
            $semanticTypeIdentifier_xml = $newline.sprintf('%s%s%s',
                    $this->generateOpenTag(self::TAG_SEMANTIC_TYPE_IDENTIFIER),
                    $this->semanticTypeIdentifier,
                    $this->generateCloseTag(self::TAG_SEMANTIC_TYPE_IDENTIFIER));

        $xml = sprintf(($prettify) ? "%s%s%s%s%s%s\n%s" : '%s%s%s%s%s%s%s',
            $this->generateOpenTag(self::TAG_FEATURE_TYPE_STYLE),
            preg_replace("/\n/", "\n\t", $name_xml),
            preg_replace("/\n/", "\n\t", $description_xml),
            preg_replace("/\n/", "\n\t", $featureTypeName_xml),
            preg_replace("/\n/", "\n\t", $semanticTypeIdentifier_xml),
            preg_replace("/\n/", "\n\t", $rules_xml),
            $this->generateCloseTag(self::TAG_FEATURE_TYPE_STYLE));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}