<?php
/**
 * Marks are predefined vector shapes identified by a well-known name. Their fill and stroke can be defined explicitly
 * in the SLD.
 *
 * User: Anos Kuda Mhazo
 * Date: 1/9/2018
 * Time: 3:40 PM
 */

namespace OGC\SLD\SE;


use function GuzzleHttp\Psr7\str;

class Mark extends SE
{

    const TAG_MARK = 'Mark';

    /**
     * @var WellKnownName
     */
    private $wellKnownName;

    /**
     * @var null|Fill
     */
    private $fill;

    /**
     * @var null|Stroke
     */
    private $stroke;


    public function __construct(WellKnownName $wellKnownName, ?Fill $fill = null, ?Stroke $stroke = null)
    {

        parent::__construct();
        $this->wellKnownName = $wellKnownName;
        $this->fill = $fill;
        $this->stroke = $stroke;

    }


    public function setWellKnownName(WellKnownName $wellKnownName) : self {
        $this->wellKnownName = $wellKnownName;
        return $this;
    }


    public function setFill(?Fill $fill = null) : self {
        $this->fill = $fill;
        return $this;
    }


    public function setStroke(?Stroke $stroke = null) : self {
        $this->stroke = $stroke;
        return $this;
    }


    public function toXML(bool $prettify = false): string
    {

        $wellKnownName_xml = '';
        $fill_xml = '';
        $stroke_xml = '';

        //New line char
        $newline = $prettify ? "\n" : "";

        //Well Known Name
        if($this->wellKnownName)
            $wellKnownName_xml = $newline.$this->wellKnownName->toXML($prettify);

        //Fill
        if($this->fill)
            $fill_xml = $newline.$this->fill->toXML($prettify);

        //Stroke
        if($this->stroke)
            $stroke_xml = $newline.$this->stroke->toXML($prettify);


        $xml = sprintf(($prettify) ? "%s%s%s%s\n%s" : '%s%s%s%s%s',
            $this->generateOpenTag(self::TAG_MARK),
            preg_replace("/\n/", "\n\t", $wellKnownName_xml),
            preg_replace("/\n/", "\n\t", $fill_xml),
            preg_replace("/\n/", "\n\t", $stroke_xml),
            $this->generateCloseTag(self::TAG_MARK));


        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }
}