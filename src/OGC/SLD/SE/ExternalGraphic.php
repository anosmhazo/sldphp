<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/9/2018
 * Time: 3:39 PM
 */

namespace OGC\SLD\SE;


class ExternalGraphic extends SE
{

    const TAG_EXTERNAL_GRAPHIC = 'ExternalGraphic';

    /**
     * @var OnlineResource
     */
    private $onlineResource;

    /**
     * @var null|Format
     */
    private $format;


    public function __construct(OnlineResource $onlineResource, ?Format $format = null)
    {

        parent::__construct();
        $this->onlineResource = $onlineResource;
        $this->format = $format;

    }


    public function toXML(bool $prettify = false): string
    {

        $onlineResource_xml = '';
        $format_xml = '';
        $newline = ($prettify) ? "\n" : '';

        if($this->onlineResource)
            $onlineResource_xml = $newline.$this->onlineResource->toXML($prettify);

        if($this->format)
            $format_xml = $newline.$this->format->toXML($prettify);

        return sprintf(($prettify) ? "%s%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_EXTERNAL_GRAPHIC),
            preg_replace("/\n/", "\n\t", $onlineResource_xml),
            preg_replace("/\n/", "\n\t", $format_xml),
            $this->generateCloseTag(self::TAG_EXTERNAL_GRAPHIC));

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}