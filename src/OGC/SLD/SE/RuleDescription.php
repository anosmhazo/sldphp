<?php
/**
 * Description for Rule. Description according to SE OGC standard comprises of title and abstract.
 * User: Anos Kuda Mhazo
 * Date: 12/28/2017
 * Time: 9:42 AM
 */

namespace OGC\SLD\SE;


use OGC\XML;

class RuleDescription extends SE
{

    const TAG_DESCRIPTION = 'Description';
    const TAG_TITLE = 'Title';
    const TAG_ABSTRACT = 'Abstract';

    /**
     * @var null|string
     */
    private $title;

    /**
     * @var null|string
     */
    private $abstract;

    /**
     * @var bool
     */
    private $usePlain = false;


    public function __construct(?string $title, ?string $abstract = null)
    {

        parent::__construct();

        $this->title = $title;
        $this->abstract = $abstract;

    }


    public function setTitle(?string $title): self{
        $this->title = $title;
        return $this;
    }


    public function setAbstract(?string $abstract): self{
        $this->abstract = $abstract;
        return $this;
    }


    public function usePlain(bool $use): self{
        $this->usePlain = $use;
        return $this;
    }


    public function toXML(bool $prettify = false): string
    {

        $xml = '';
        $title_xml = '';
        $abstract_xml = '';

        if($this->title)
            $title_xml = sprintf(
                ($prettify && !$this->usePlain) ? "\n%s%s%s" : '%s%s%s',
                $this->generateOpenTag(self::TAG_TITLE),
                $this->title,
                $this->generateCloseTag(self::TAG_TITLE)
            );

        if($this->abstract)
            $abstract_xml = sprintf(
                ($prettify && (strlen($title_xml) > 0 || !$this->usePlain) ) ? "\n%s%s%s" : '%s%s%s',
                $this->generateOpenTag(self::TAG_ABSTRACT),
                $this->abstract,
                $this->generateCloseTag(self::TAG_ABSTRACT)
            );

        if($this->usePlain){

            $xml = sprintf("%s%s",$title_xml, $abstract_xml);

        }else{

            $xml = sprintf(($prettify) ? "%s%s%s\n%s" : '%s%s%s%s',
                $this->generateOpenTag(self::TAG_DESCRIPTION),
                preg_replace("/\n/", "\n\t", $title_xml),
                preg_replace("/\n/", "\n\t", $abstract_xml),
                $this->generateCloseTag(self::TAG_DESCRIPTION));

        }


        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}