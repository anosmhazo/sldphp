<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/12/2018
 * Time: 3:14 PM
 */

namespace OGC\SLD\SE;


use OGC\SLD\FE\Expression;

class PerpendicularOffset extends SE
{


    const TAG = 'PerpendicularOffset';

    /**
     * @var
     */
    private $value;

    /**
     * @var Expression
     */
    private $expression;


    protected function __construct()
    {
        parent::__construct();
    }


    /**
     * Creates parameter from primitive value
     * @param float $value
     * @return PerpendicularOffset
     */
    public static function fromValue(float $value): self{

        $self = new self();
        $self->value = $value;
        return $self;

    }


    /**
     * Creates parameter from expression
     * @param Expression $expression
     * @return PerpendicularOffset
     */
    public static function fromExpression(Expression $expression): self{

        $self = new self();
        $self->expression = $expression;
        return $self;

    }


    public function toXML(bool $prettify = false): string
    {

        if($this->value !== null){

            $xml = sprintf('%s%s%s',
                $this->generateOpenTag(self::TAG),
                $this->value,
                $this->generateCloseTag(self::TAG));

        }else{

            $xml = sprintf(($prettify) ? "%s%s\n%s" : '%s%s%s',
                $this->generateOpenTag(self::TAG),
                preg_replace("/\n/", "\n\t", $this->expression->toXML($prettify)),
                $this->generateCloseTag(self::TAG));

        }

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}