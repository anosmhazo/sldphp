<?php
/**
 * The <Fill> element specifies the styling for the interior of a polygon
 *
 * User: Anos Kuda Mhazo
 * Date: 1/11/2018
 * Time: 11:46 AM
 */

namespace OGC\SLD\SE;


use OGC\SLD\Style;

class Fill extends SE
{

    const TAG_FILL = 'Fill';

    /**
     * @var null|GraphicFill
     */
    private $graphicFill;

    /**
     * @var array|StyleParameter[]
     */
    private $parameters = [];


    public function __construct(?GraphicFill $graphicFill = null, StyleParameter ...$parameters)
    {

        parent::__construct();

        $this->graphicFill = $graphicFill;
        $this->parameters = $parameters;

        //Disable namespace by default
        $this->applyNamespace(false);

    }


    public function setGraphicFill(?GraphicFill $graphicFill = null): self {

        $this->graphicFill = $graphicFill;
        return $this;

    }


    public function addStyleParameters(StyleParameter ...$parameters): self{

        $this->parameters = array_merge($this->parameters, $parameters);
        return $this;

    }


    public function removeStyleParameters(int $from, int $len = -1): self{

        $len = ($len < 0) ? count($this->parameters) : $len;
        array_splice($this->parameters, $from, $len);
        return $this;

    }


    private function parametersToXml(bool $prettify){

        //Reduce array to XML string
        return array_reduce($this->parameters, function($carry, $parameters) use ($prettify){

            $carry .= strlen($carry) && $prettify ? "\n" : "";
            $carry .= $parameters->toXML($prettify);
            return $carry;

        }, '');

    }


    public function toXML(bool $prettify = false): string
    {
        $graphicFill_xml = '';
        $parameters_xml = '';
        $newline = $prettify ? "\n" : "";

        //Graphic fill
        if($this->graphicFill)
            $graphicFill_xml = $newline.$this->graphicFill->toXML($prettify);

        //Parameters
        if($this->parameters)
            $parameters_xml = $newline.$this->parametersToXml($prettify);


        $xml = sprintf(($prettify) ? "%s%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_FILL),
            preg_replace("/\n/", "\n\t", $graphicFill_xml),
            preg_replace("/\n/", "\n\t", $parameters_xml),
            $this->generateCloseTag(self::TAG_FILL));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}