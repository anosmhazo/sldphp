<?php
/**
 * Styles linear geometic features with a stroke
 *
 * User: Anos Kuda Mhazo
 * Date: 12/27/2017
 * Time: 2:39 PM
 */

namespace OGC\SLD\SE;


class LineSymbolizer extends Symbolizer
{

    const TAG_LINE_SYMBOLIZER = 'LineSymbolizer';

    /**
     * @var null|Geometry
     */
    private $geometry;

    /**
     * @var null|Stroke
     */
    private $stroke;

    /**
     * @var null|PerpendicularOffset
     */
    private $perpendicularOffset;


    public function __construct(?Geometry $geometry = null, ?Stroke $stroke = null, ?PerpendicularOffset $offset = null)
    {

        parent::__construct();
        $this->geometry = $geometry;
        $this->stroke = $stroke;
        $this->perpendicularOffset = $offset;
    }


    public function setPerpendicularOffset(?PerpendicularOffset $offset): self{

        $this->perpendicularOffset = $offset;
        return $this;

    }


    public function setGeometry(?Geometry $geometry): self{

        $this->geometry = $geometry;
        return $this;

    }


    public function setStroke(?Stroke $stroke): self{

        $this->stroke = $stroke;
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        $stroke = '';
        $offset = '';
        $geometry = '';

        $newline = $prettify ? "\n" : '';

        if($this->stroke)
            $stroke = $newline.$this->stroke->toXML($prettify);
        if($this->geometry)
            $geometry = $newline.$this->geometry->toXML($prettify);
        if($this->perpendicularOffset)
            $offset = $newline.$this->perpendicularOffset->toXML($prettify);


        $xml = sprintf(($prettify) ? "%s%s%s%s\n%s" : '%s%s%s%s%s',
            $this->generateOpenTag(self::TAG_LINE_SYMBOLIZER),
            preg_replace("/\n/", "\n\t", $stroke),
            preg_replace("/\n/", "\n\t", $geometry),
            preg_replace("/\n/", "\n\t", $offset),
            $this->generateCloseTag(self::TAG_LINE_SYMBOLIZER));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}