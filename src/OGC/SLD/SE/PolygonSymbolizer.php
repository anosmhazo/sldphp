<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/27/2017
 * Time: 2:39 PM
 */

namespace OGC\SLD\SE;


class PolygonSymbolizer extends Symbolizer
{


    const TAG_POLYGON_SYMBOLIZER = 'PolygonSymbolizer';

    /**
     * @var null|Geometry
     */
    private $geometry;

    /**
     * @var null|Fill
     */
    private $fill;

    /**
     * @var null|Stroke
     */
    private $stroke;


    public function __construct(?Geometry $geometry = null, ?Fill $fill = null, ?Stroke $stroke = null)
    {

        parent::__construct();

        $this->geometry = $geometry;
        $this->fill = $fill;
        $this->stroke = $stroke;

    }


    public function setGeometry(?Geometry $geometry): self {

        $this->geometry = $geometry;
        return $this;

    }


    public function setFill(?Fill $fill): self {

        $this->fill = $fill;
        return $this;

    }


    public function setStroke(?Stroke $stroke): self {

        $this->stroke = $stroke;
        return $this;

    }


    public function toXML(bool $prettify = false): string
    {

        $stroke = '';
        $fill = '';
        $geometry = '';

        $newline = $prettify ? "\n" : "";

        if($this->geometry)
            $geometry = $newline.$this->geometry->toXML($prettify);
        if($this->fill)
            $fill = $newline.$this->fill->toXML($prettify);
        if($this->stroke)
            $stroke = $newline.$this->stroke->toXML($prettify);

        $xml = sprintf(($prettify) ? "%s%s%s%s\n%s" : '%s%s%s%s%s',
            $this->generateOpenTag(self::TAG_POLYGON_SYMBOLIZER),
            preg_replace("/\n/", "\n\t", $geometry),
            preg_replace("/\n/", "\n\t", $fill),
            preg_replace("/\n/", "\n\t", $stroke),
            $this->generateCloseTag(self::TAG_POLYGON_SYMBOLIZER));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}