<?php
/**
 * A TextSymbolizer styles features as text labels. Text labels are positioned eoither at points or along linear paths
 * derived from the geometry being labelled. Labelling is a complex operation, and effective labelling is crucial to
 * obtaining legible and visually pleasing cartographic output. For this reason SLD provides many options to control
 * label placement. To improve quality even more GeoServer provides additional options and parameters.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/27/2017
 * Time: 2:40 PM
 */

namespace OGC\SLD\SE;


use OGC\SLD\SE\GeoServer\VendorOption;

class TextSymbolizer extends Symbolizer
{

    const TAG_TEXT_SYMBOLIZER = 'TextSymbolizer';


    private $geometry;
    private $label;
    private $font;
    private $labelPlacement;
    private $halo;
    private $fill;
    private $graphic;
    private $priority;
    private $vendorOptions;


    public function __construct(?Geometry $geometry = null, ?Label $label = null, ?Font $font = null,
                                ?LabelPlacement $labelPlacement = null, ?Halo $halo = null, ?Fill $fill = null,
                                ?Graphic $graphic = null, ?Priority $priority = null, ?VendorOption ...$vendorOptions)
    {

        parent::__construct();

        $this->geometry = $geometry;
        $this->label = $label;
        $this->font = $font;
        $this->labelPlacement = $labelPlacement;
        $this->halo = $halo;
        $this->fill = $fill;
        $this->graphic = $graphic;
        $this->priority = $priority;
        $this->vendorOptions = $vendorOptions;

    }


    public function setGeometry(?Geometry $geometry = null): self {
        $this->geometry = $geometry;
        return $this;
    }


    public function setLabel(?Label $label = null): self {
        $this->label = $label;
        return $this;
    }


    public function setFont(?Font $font = null): self {
        $this->font = $font;
        return $this;
    }


    public function setLabelPlacement(?LabelPlacement $labelPlacement = null): self {
        $this->labelPlacement = $labelPlacement;
        return $this;
    }


    public function setHalo(?Halo $halo = null): self {
        $this->halo = $halo;
        return $this;
    }


    public function setFill(?Fill $fill = null): self {
        $this->fill = $fill;
        return $this;
    }


    public function setGraphic(?Graphic $graphic = null): self {
        $this->graphic = $graphic;
        return $this;
    }


    public function setPriority(?Priority $priority = null): self {
        $this->priority = $priority;
        return $this;
    }


    public function setVendorOptions(?VendorOption ...$vendorOptions): self {
        $this->vendorOptions = array_merge($this->vendorOptions, $vendorOptions);
        return $this;
    }


    private function vendorOptionsToXml(bool $prettify){

        //Reduce array to XML string
        return array_reduce($this->vendorOptions, function($carry, $options) use ($prettify){

            $carry .= strlen($carry) && $prettify ? "\n" : "";
            $carry .= $options->toXML($prettify);
            return $carry;

        }, '');

    }


    public function toXML(bool $prettify = false): string
    {

        $geometry_xml = '';
        $label_xml = '';
        $font_xml = '';
        $labelPlacement_xml = '';
        $halo_xml = '';
        $fill_xml = '';
        $graphic_xml = '';
        $priority_xml = '';
        $vendorOptions_xml = '';

        $newline = $prettify ? "\n" : "";

        //Geometry
        if($this->geometry)
            $geometry_xml = $newline.$this->geometry->toXML($prettify);

        //Label
        if($this->label)
            $label_xml = $newline.$this->label->toXML($prettify);

        //Font
        if($this->font)
            $font_xml = $newline.$this->font->toXML($prettify);

        //Label Placement
        if($this->labelPlacement)
            $labelPlacement_xml = $newline.$this->labelPlacement->toXML($prettify);

        //Halo
        if($this->halo)
            $halo_xml = $newline.$this->halo->toXML($prettify);

        //Fill
        if($this->fill)
            $fill_xml = $newline.$this->fill->toXML($prettify);

        //Graphic
        if($this->graphic)
            $graphic_xml = $newline.$this->graphic->toXML($prettify);

        //Priority
        if($this->priority)
            $priority_xml = $newline.$this->priority->toXML($prettify);

        //Vendor Options
        if($this->vendorOptions)
            $vendorOptions_xml = $newline.$this->vendorOptions->toXML($prettify);

        $xml = sprintf(($prettify) ? "%s%s%s%s%s%s%s%s%s%s\n%s" : '%s%s%s%s%s%s%s%s%s%s%s',
            $this->generateOpenTag(self::TAG_TEXT_SYMBOLIZER),
            preg_replace("/\n/", "\n\t", $geometry_xml),
            preg_replace("/\n/", "\n\t", $label_xml),
            preg_replace("/\n/", "\n\t", $font_xml),
            preg_replace("/\n/", "\n\t", $labelPlacement_xml),
            preg_replace("/\n/", "\n\t", $halo_xml),
            preg_replace("/\n/", "\n\t", $fill_xml),
            preg_replace("/\n/", "\n\t", $graphic_xml),
            preg_replace("/\n/", "\n\t", $priority_xml),
            preg_replace("/\n/", "\n\t", $vendorOptions_xml),
            $this->generateCloseTag(self::TAG_TEXT_SYMBOLIZER));

        //Remove excess line break \n characters
        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}