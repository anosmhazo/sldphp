<?php
/**
 * A halo creates a colored background around the label text, which improves readability in low contrast situations.
 * Within the <Halo> element there are two sub-elements which control the appearance of the halo.
 *
 * User: Anos Kuda Mhazo
 * Date: 1/12/2018
 * Time: 4:11 PM
 */

namespace OGC\SLD\SE;


class Halo extends SE
{

    const TAG_HALO = 'Halo';

    private $radius;
    private $fill;


    public function __construct(?Radius $radius = null, ?Fill $fill = null)
    {

        parent::__construct();

        $this->radius = $radius;
        $this->fill = $fill;

    }


    public function setRadius(?Radius $radius = null): self {

        $this->radius = $radius;
        return $this;

    }


    public function setFill(?Fill $fill = null): self {

        $this->fill = $fill;
        return $this;

    }

    public function toXML(bool $prettify = false): string
    {

        $radius_xml = '';
        $fill_xml = '';

        if($this->radius)
            $radius_xml = $this->radius->toXML($prettify);

        if($this->fill)
            $fill_xml = $this->fill->toXML($prettify);

        return sprintf(($prettify) ? "%s%s%s\n%s" : '%s%s%s%s',
            $this->generateOpenTag(self::TAG_HALO),
            preg_replace("/\n/", "\n\t", $radius_xml),
            preg_replace("/\n/", "\n\t", $fill_xml),
            $this->generateCloseTag(self::TAG_HALO));

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}