<?php
/**
 * 	The xlink:href attribute specifies the location of the image file. The value can be either a URL or a local pathname
 *  relative to the SLD directory. The value can contain CQL expressions delimited by ${ }. The attribute
 * xlink:type="simple" is also required. The element does not contain any content.
 *
 * User: Anos Kuda Mhazo
 * Date: 1/9/2018
 * Time: 3:45 PM
 */

namespace OGC\SLD\SE;


class OnlineResource extends SE
{

    const TAG_ONLINE_RESOURCE = 'OnlineResource';

    const ATTR_HREF = 'xlink:href';
    const ATTR_TYPE = 'xlink:type';

    private $href;
    private $type;

    public function __construct(string $href, string $type = 'simple')
    {

        parent::__construct();

        $this->href = $href;
        $this->type = $type;

    }


    public function toXML(bool $prettify = false): string
    {

        $attributes = [
            self::ATTR_TYPE => $this->type,
            self::ATTR_HREF => $this->href
        ];

        return self::generateOpenSelfClosingTag(self::TAG_ONLINE_RESOURCE, $attributes);

    }


    public function __toString()
    {
       return $this->toXML(true);
    }

}