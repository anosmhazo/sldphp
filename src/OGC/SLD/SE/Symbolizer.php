<?php
/**
 * A symbolizer describes how a feature should be displayed on a map. It describes not only the shape but also
 * graphical properties such as color and opacity.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/27/2017
 * Time: 2:38 PM
 */

namespace OGC\SLD\SE;


use OGC\XML;

abstract class Symbolizer extends SE
{

    const ATTR_UOM = 'gml:uom';

    const UOM_METRE = 'metre';
    const UOM_FOOT = 'foot';
    const UOM_PIXEL = 'pixel';

    private $uom;


    protected function __construct()
    {
        parent::__construct();
    }


    protected function generateAttributes(): array{

        $attributes = [];

        if($this->uom)
            $attributes[self::ATTR_UOM] = $this->uom;

        return $attributes;

    }

    public function setUnitOfMeasure(?string $uom): self{
        $this->uom = $uom;
        return $this;
    }

}