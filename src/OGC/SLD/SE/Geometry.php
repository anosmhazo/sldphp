<?php
/**
 * Defines geometry to be used for styling
 *
 * User: Anos Kuda Mhazo
 * Date: 1/3/2018
 * Time: 10:28 AM
 */

namespace OGC\SLD\SE;


use OGC\SLD\FE\Expression;

class Geometry extends SE
{

    const TAG_GEOMETRY = 'Geometry';
    public $expression;

    public function __construct(Expression $expression)
    {

        parent::__construct();
        $this->expression = $expression;
    }

    public function toXML(bool $prettify = false): string
    {


        $xml = sprintf(($prettify) ? "%s\n\t%s\n%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_GEOMETRY),
            preg_replace("/\n/", "\n\t", $this->expression->toXML($prettify)),
            $this->generateCloseTag(self::TAG_GEOMETRY));

        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}