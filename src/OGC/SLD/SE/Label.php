<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/12/2018
 * Time: 1:28 PM
 */

namespace OGC\SLD\SE;


use OGC\SLD\FE\Expression;

class Label extends SE
{

    const TAG_LABEL = 'Label';

    private $expressions;
    private $format;


    public function __construct(?string $format, Expression ...$expressions)
    {

        parent::__construct();

        $this->format = $format;
        $this->expressions = $expressions;

    }

    private function expressionsToXml(bool $prettify){

        //Reduce array to XML string
        return array_reduce($this->expressions, function($carry, $expression) use ($prettify){

            $carry .= strlen($carry) && $prettify ? "\n" : "";
            $carry .= $expression->toXML($prettify);
            return $carry;

        }, '');

    }

    private function expressionsToXmlArray(bool $prettify){

        //Reduce array to XML string
        return array_map(function($expression) use ($prettify){

            return $expression->toXML($prettify);

        }, $this->expressions);

    }

    public function toXML(bool $prettify = false): string
    {

        $newline = $prettify ? "\n" : '';

        if($this->format)
            $expression_xml = $newline.sprintf($this->format, ...$this->expressionsToXmlArray($prettify));
        else
            $expression_xml = $newline.$this->expressionsToXml($prettify);

        return sprintf(($prettify) ? "%s%s\n%s" : '%s%s%s',
            $this->generateOpenTag(self::TAG_LABEL),
            preg_replace("/\n/", "\n\t", $expression_xml),
            $this->generateCloseTag(self::TAG_LABEL));

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}