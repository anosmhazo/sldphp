<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/12/2018
 * Time: 2:08 PM
 */

namespace OGC\SLD\SE;


class PointPlacement extends SE
{

    const TAG_POINT_PLACEMENT = 'PointPlacement';

    /**
     * @var null|AnchorPoint
     */
    private $anchorPoint;

    /**
     * @var null|Displacement
     */
    private $displacement;

    /**
     * @var null|Rotation
     */
    private $rotation;


    public function __construct(?AnchorPoint $anchorPoint = null, ?Displacement $displacement = null, ?Rotation $rotation = null)
    {

        parent::__construct();

        $this->anchorPoint = $anchorPoint;
        $this->displacement = $displacement;
        $this->rotation = $rotation;

    }


    public function setAnchorPoint(?AnchorPoint $anchorPoint = null): self {

        $this->anchorPoint = $anchorPoint;
        return $this;

    }


    public function setDisplacement(?Displacement $displacement): self {

        $this->displacement = $displacement;
        return $this;

    }


    public function setRotation(?Rotation $rotation): self {

        $this->rotation = $rotation;
        return $this;

    }

    public function toXML(bool $prettify = false): string
    {

        $anchorPoint_xml = '';
        $displacement_xml = '';
        $rotation_xml = '';


        $newline = $prettify ? "\n" : "";

        //Anchor point
        if($this->anchorPoint)
            $anchorPoint_xml = $newline.$this->anchorPoint->toXML($prettify);

        //Displacement
        if($this->displacement)
            $displacement_xml = $newline.$this->displacement->toXML($prettify);

        //Rotation
        if($this->rotation)
            $rotation_xml = $newline.$this->rotation->toXML($prettify);

        return sprintf(($prettify) ? "%s%s%s%s\n%s" : '%s%s%s%s%s',
            $this->generateOpenTag(self::TAG_POINT_PLACEMENT),
            preg_replace("/\n/", "\n\t", $anchorPoint_xml),
            preg_replace("/\n/", "\n\t", $displacement_xml),
            preg_replace("/\n/", "\n\t", $rotation_xml),
            $this->generateCloseTag(self::TAG_POINT_PLACEMENT));

    }


    public function __toString()
    {
        return $this->toXML(true);
    }

}