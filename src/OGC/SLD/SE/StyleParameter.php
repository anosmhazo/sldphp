<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/3/2018
 * Time: 10:50 AM
 */

namespace OGC\SLD\SE;

abstract class StyleParameter extends SE
{

    const ATTR_NAME = 'name';

    //LINES

    const NAME_STROKE_COLOR = 'stroke';
    const NAME_STROKE_OPACITY = 'stroke-opacity';
    const NAME_STROKE_WIDTH = 'stroke-width';
    const NAME_STROKE_LINE_JOIN = 'stroke-linejoin';
    const NAME_STROKE_LINE_CAP = 'stroke-linecap';
    const NAME_STROKE_DASH_ARRAY = 'stroke-dasharray';
    const NAME_STROKE_DASH_OFFSET = 'stroke-dashoffset';

    //POLYGONS

    const NAME_FILL_COLOR = 'fill';
    const NAME_FILL_OPACITY = 'fill-opacity';


    //FONTS

    const NAME_FONT_FAMILY = 'font-family';
    const NAME_FONT_STYLE = 'font-style';
    const NAME_FONT_WEIGHT = 'font-weight';
    const NAME_FONT_SIZE = 'font-size';

    protected $name;

    protected function generateAttributes(): array{

        $attributes = [];

        if($this->name)
            $attributes[self::ATTR_NAME] = $this->name;

        return $attributes;

    }

}