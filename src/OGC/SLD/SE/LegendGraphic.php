<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/11/2017
 * Time: 3:57 PM
 */

namespace OGC\SLD\SE;


class LegendGraphic extends SE
{


    const TAG_LEGEND_GRAPHIC = 'LegendGraphic';

    /**
     * @var Graphic
     */
    private $graphic;


    public function __construct(Graphic $graphic)
    {

        parent::__construct();
        $this->graphic = $graphic;

    }


    public function toXML(bool $prettify = false): string
    {

        return sprintf($prettify ? "%s\n\t%s%s" : '%s%s%s',
            self::generateOpenTag(self::TAG_LEGEND_GRAPHIC),
            preg_replace("/\n/", "\n\t", $this->graphic->toXML($prettify)),
            self::generateCloseTag(self::TAG_LEGEND_GRAPHIC));

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}