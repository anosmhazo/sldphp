<?php
/**
 * Rule is used to group rendering instructions by feature-property conditions and map scales.
 * Ordering of rules in feature type style follows the painters model with the first rule being plotted
 * at the bottom most.
 *
 * User: Anos Kuda Mhazo
 * Date: 12/11/2017
 * Time: 3:11 PM
 */

namespace OGC\SLD\SE;


use OGC\SLD\FE\Filter;
use OGC\XML;

class Rule extends SE
{

    const TAG_RULE = 'Rule';

    const TAG_NAME = 'Name';
    const TAG_MIN_SCALE_DENOMINATOR = 'MinScaleDenominator';
    const TAG_MAX_SCALE_DENOMINATOR = 'MaxScaleDenominator';

    /**
     * @var null|string
     */
    private $name;

    /**
     * @var null|RuleDescription
     */
    private $description;

    /**
     * @var LegendGraphic
     */
    private $legendGraphic;

    /**
     * @var null|Filter
     */
    private $filter;

    /**
     * @var float
     */
    private $minScaleDenominator;

    /**
     * @var float
     */
    private $maxScaleDenominator;

    /**
     * @var array <Symbolizer>
     */
    private $symbolizers = [];


    public function __construct(?string $name = null, ?RuleDescription $description = null, ?Filter $filter = null, Symbolizer ...$symbolizers)
    {

        parent::__construct();

        $this->name = $name;
        $this->description = $description;
        $this->filter = $filter;
        $this->symbolizers = $symbolizers;

    }


    /**
     * Gets name of rule to be used as external reference.
     * @return string|null
     */
    public function getName(): ?string{

        return $this->name;

    }


    /**
     * Gets description of rule. This can be used as the title of the rule
     * so that it can be displayed in a map legend. The XML for Feature Type Style is defined on page 5 of OGC standard 05-077r4 Symbology Encoding.
     * @return string|null
     */
    public function getDescription(): ?string{

        return $this->description;

    }


    public function getLegendGraphic(): ?LegendGraphic{
        return $this->legendGraphic;
    }


    /**
     * Gets minimum scale denominator. Minimum scale is inclusive.
     * @return int|null
     */
    public function getMinScaleDenominator(): ?int{

        return $this->minScaleDenominator;

    }


    /**
     * Gets maximum scale denominator. Maximum scale is exclusive.
     * @return int|null
     */
    public function getMaxScaleDenominator(): ?int{

        return $this->maxScaleDenominator;

    }


    public function setName(?string $name): self{
        $this->name = $name;
        return $this;
    }


    public function setDescription(?RuleDescription $description): self{
        $this->description = $description;
        return $this;
    }


    public function setMinScaleDenominator(?float $minScale): self{
        $this->minScaleDenominator = $minScale;
        return $this;
    }


    public function setMaxScaleDenominator(?float $maxScale): self{
        $this->maxScaleDenominator = $maxScale;
        return $this;
    }


    public function setFilter(?Filter $filter): self{
        $this->filter = $filter;
        return $this;
    }


    public function setLegendGraphic(?LegendGraphic $legendGraphic): self{
        $this->legendGraphic = $legendGraphic;
        return $this;
    }


    public function addSymbolizers(Symbolizer ...$symbolizers): self{

        $this->symbolizers = array_merge($this->symbolizers, $symbolizers);
        return $this;

    }


    public function removeSymbolizers(int $from, int $len = -1): self{

        $len = ($len < 0) ? count($this->symbolizers) : $len;
        array_splice($this->symbolizers, $from, $len);
        return $this;

    }


    private function symbolizersToXml(bool $prettify){

        //Reduce array to XML string
        return array_reduce($this->symbolizers, function($carry, $symbolizer) use ($prettify){

            $carry .= strlen($carry) && $prettify ? "\n" : "";
            $carry .= $symbolizer->toXML($prettify);
            return $carry;

        }, '');

    }


    public function toXML(bool $prettify = false): string
    {

        $name_xml = '';
        $description_xml = '';
        $filter_xml = '';
        $symbolizer_xml = '';
        $maxScaleDenominator_xml = '';
        $minScaleDenominator_xml = '';
        $legendGraphic_xml = '';

        $newline = $prettify ? "\n" : "";

        //Name
        if($this->name)
            $name_xml = $newline.sprintf('%s%s%s',
                    $this->generateOpenTag(self::TAG_NAME),
                    $this->name,
                    $this->generateCloseTag(self::TAG_NAME));

        //Description
        if($this->description)
            $description_xml = $newline.$this->description->toXML($prettify);

        //Filter
        if($this->filter)
            $filter_xml = $newline.$this->filter->toXML($prettify);

        //Min Scale Denominator
        if(is_float($this->minScaleDenominator))
            $minScaleDenominator_xml = $newline.sprintf('%s%s%s',
                $this->generateOpenTag(self::TAG_MIN_SCALE_DENOMINATOR),
                $this->minScaleDenominator,
                $this->generateCloseTag(self::TAG_MIN_SCALE_DENOMINATOR));

        //Max Scale Denominator
        if(is_float($this->maxScaleDenominator))
            $maxScaleDenominator_xml = $newline.sprintf('%s%s%s',
                $this->generateOpenTag(self::TAG_MAX_SCALE_DENOMINATOR),
                $this->maxScaleDenominator,
                $this->generateCloseTag(self::TAG_MAX_SCALE_DENOMINATOR));

        //Symbolizers
        if($this->symbolizers)
            $symbolizer_xml = $newline.$this->symbolizersToXml($prettify);

        //Legend graphic
        if($this->legendGraphic)
            $legendGraphic_xml = $newline.$this->legendGraphic->toXML($prettify);

        $xml = sprintf(($prettify) ? "%s%s%s%s%s%s%s%s\n%s" : '%s%s%s%s%s%s%s%s%s',
            $this->generateOpenTag(self::TAG_RULE),
            preg_replace("/\n/", "\n\t", $name_xml),
            preg_replace("/\n/", "\n\t", $description_xml),
            preg_replace("/\n/", "\n\t", $filter_xml),
            preg_replace("/\n/", "\n\t", $minScaleDenominator_xml),
            preg_replace("/\n/", "\n\t", $maxScaleDenominator_xml),
            preg_replace("/\n/", "\n\t", $legendGraphic_xml),
            preg_replace("/\n/", "\n\t", $symbolizer_xml),
            $this->generateCloseTag(self::TAG_RULE));

        //Remove excess line break \n characters
        return $xml;

    }


    public function __toString()
    {
        return $this->toXML(true);
    }


}