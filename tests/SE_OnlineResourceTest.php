<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/10/2018
 * Time: 12:28 PM
 */

use OGC\SLD\SE\OnlineResource;
use PHPUnit\Framework\TestCase;

class SE_OnlineResourceTest extends TestCase
{

    const STANDARD_XML = '<OnlineResource xlink:type="simple" xlink:href="file:///var/www/htdocs/sun.png" />';


    /**
     * @test
     */
    public function testCanOutputStandardXML(){

        $resource = new OnlineResource('file:///var/www/htdocs/sun.png');
        $this->assertSame(self::STANDARD_XML, $resource->applyNamespace(false)->toXML());

    }


}
