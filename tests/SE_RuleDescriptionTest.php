<?php

use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 12/28/2017
 * Time: 11:44 AM
 */


class SE_RuleDescriptionTest extends TestCase
{

    //Prettified
    const STANDARD_XML_PRETTIFIED = "<Description>\n</Description>";
    const TITLE_XML_PRETTIFIED = "<Description>\n\t<Title>Sample Style</Title>\n</Description>";
    const ABSTRACT_XML_PRETTIFIED = "<Description>\n\t<Abstract>Sample Style Abstract</Abstract>\n</Description>";
    const TITLE_ABSTRACT_XML_PRETTIFIED = "<Description>\n\t<Title>Sample Style</Title>\n\t<Abstract>Sample Style Abstract</Abstract>\n</Description>";

    //Plain
    const STANDARD_XML = "<Description></Description>";
    const TITLE_XML = "<Description><Title>Sample Style</Title></Description>";
    const ABSTRACT_XML = "<Description><Abstract>Sample Style Abstract</Abstract></Description>";
    const TITLE_ABSTRACT_XML = "<Description><Title>Sample Style</Title><Abstract>Sample Style Abstract</Abstract></Description>";

    const TEST_TITLE = 'Sample Style';
    const TEST_ABSTRACT = 'Sample Style Abstract';

    /**
     * @test
     */
    public function testCanOutputStandardXMLPrettified(){

        $description = new \OGC\SLD\SE\RuleDescription(null);
        $this->assertSame(self::STANDARD_XML_PRETTIFIED, $description->__toString());

    }


    /**
     * @test
     */
    public function testCanOutputTitleXMLPrettified(){

        $description = new \OGC\SLD\SE\RuleDescription(self::TEST_TITLE);
        $this->assertSame(self::TITLE_XML_PRETTIFIED, $description->__toString());

    }


    /**
     * @test
     */
    public function testCanOutputAbstractXMLPrettified(){

        $description = new \OGC\SLD\SE\RuleDescription(null,self::TEST_ABSTRACT);
        $this->assertSame(self::ABSTRACT_XML_PRETTIFIED, $description->__toString());

    }


    /**
     * @test
     */
    public function testCanOutputTitleAbstractXMLPrettified(){

        $description = new \OGC\SLD\SE\RuleDescription(self::TEST_TITLE,self::TEST_ABSTRACT);
        $this->assertSame(self::TITLE_ABSTRACT_XML_PRETTIFIED, $description->__toString());

    }

    /**
     * @test
     */
    public function testCanOutputStandardXML(){

        $description = new \OGC\SLD\SE\RuleDescription(null);
        $this->assertSame(self::STANDARD_XML, $description->toXML());

    }


    /**
     * @test
     */
    public function testCanOutputTitleXML(){

        $description = new \OGC\SLD\SE\RuleDescription(self::TEST_TITLE);
        $this->assertSame(self::TITLE_XML, $description->toXML());

    }


    /**
     * @test
     */
    public function testCanOutputAbstractXML(){

        $description = new \OGC\SLD\SE\RuleDescription(null,self::TEST_ABSTRACT);
        $this->assertSame(self::ABSTRACT_XML, $description->toXML());

    }


    /**
     * @test
     */
    public function testCanOutputTitleAbstractXML(){

        $description = new \OGC\SLD\SE\RuleDescription(self::TEST_TITLE,self::TEST_ABSTRACT);
        $this->assertSame(self::TITLE_ABSTRACT_XML, $description->toXML());

    }

}
