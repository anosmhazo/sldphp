<?php
/**
 * Tests for Format class
 * User: Anos Kuda Mhazo
 * Date: 1/10/2018
 * Time: 3:20 PM
 */

use OGC\SLD\SE\Format;
use PHPUnit\Framework\TestCase;

class SE_FormatTest extends TestCase
{

    const STANDARD_XML = '<Format>image/png</Format>';


    /**
     * @test
     */
    public function testCanOutputStandardXML(){

        $description = new Format(Format::FORMAT_PNG);
        $this->assertSame(self::STANDARD_XML, $description->toXML());

    }

}
