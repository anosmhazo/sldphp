<?php
/**
 * Graphic Class Tests
 * User: Anos Kuda Mhazo
 * Date: 1/11/2018
 * Time: 3:18 PM
 */

use OGC\SLD\SE\CssParameter;
use OGC\SLD\SE\Fill;
use OGC\SLD\SE\Graphic;
use OGC\SLD\SE\Mark;
use OGC\SLD\SE\Size;
use OGC\SLD\SE\StyleParameter;
use OGC\SLD\SE\WellKnownName;
use PHPUnit\Framework\TestCase;

class SE_GraphicTest extends TestCase
{

    const DATA_SAMPLE1 =
        '<Graphic>'.
        '<Mark>'.
        '<WellKnownName>circle</WellKnownName>'.
        '<Fill>'.
        '<CssParameter name="fill">#FF0000</CssParameter>'.
        '</Fill>'.
        '</Mark>'.
        '<Size>6</Size>'.
        '</Graphic>';

    const DATA_SAMPLE1_PRETTIFIED =
        "<Graphic>\n".
        "\t<Mark>\n".
        "\t\t<WellKnownName>circle</WellKnownName>\n".
        "\t\t<Fill>\n".
        "\t\t\t<CssParameter name=\"fill\">#FF0000</CssParameter>\n".
        "\t\t</Fill>\n".
        "\t</Mark>\n".
        "\t<Size>6</Size>\n".
        "</Graphic>";

    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputSample1XML(){

        //Well Known Name
        $wellKnownName = new WellKnownName(WellKnownName::SLD_CIRCLE);

        //Fill
        $style1 = CssParameter::fromValue(StyleParameter::NAME_FILL_COLOR, '#FF0000');
        $fill = new Fill(null, $style1);

        //Mark
        $mark = new Mark($wellKnownName, $fill);

        //Size
        $size = Size::fromValue(6);

        //Graphic
        $graphic = new Graphic(null, $mark, null, $size);


        //Assert
        $this->assertSame(self::DATA_SAMPLE1, $graphic->toXML());

    }

    
    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputSample1XMLPrettified(){

        //Well Known Name
        $wellKnownName = new WellKnownName(WellKnownName::SLD_CIRCLE);

        //Fill
        $style1 = CssParameter::fromValue(StyleParameter::NAME_FILL_COLOR, '#FF0000');
        $fill = new Fill(null, $style1);

        //Mark
        $mark = new Mark($wellKnownName, $fill);

        //Size
        $size = Size::fromValue(6);

        //Graphic
        $graphic = new Graphic(null, $mark, null, $size);


        //Assert
        $this->assertSame(self::DATA_SAMPLE1_PRETTIFIED, $graphic->toXML(true));

    }


}
