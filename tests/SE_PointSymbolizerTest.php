<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/12/2018
 * Time: 9:23 AM
 */

use OGC\SLD\SE\CssParameter;
use OGC\SLD\SE\Fill;
use OGC\SLD\SE\Graphic;
use OGC\SLD\SE\Mark;
use OGC\SLD\SE\PointSymbolizer;
use OGC\SLD\SE\Size;
use OGC\SLD\SE\StyleParameter;
use OGC\SLD\SE\WellKnownName;
use PHPUnit\Framework\TestCase;

class SE_PointSymbolizerTest extends TestCase
{

    const DATA_SAMPLE1 =
        '<PointSymbolizer>'.
        '<Graphic>'.
        '<Mark>'.
        '<WellKnownName>circle</WellKnownName>'.
        '<Fill>'.
        '<CssParameter name="fill">#FF0000</CssParameter>'.
        '</Fill>'.
        '</Mark>'.
        '<Size>6</Size>'.
        '</Graphic>'.
        '</PointSymbolizer>';

    const DATA_SAMPLE1_PRETTIFIED =
        "<PointSymbolizer>\n".
        "\t<Graphic>\n".
        "\t\t<Mark>\n".
        "\t\t\t<WellKnownName>circle</WellKnownName>\n".
        "\t\t\t<Fill>\n".
        "\t\t\t\t<CssParameter name=\"fill\">#FF0000</CssParameter>\n".
        "\t\t\t</Fill>\n".
        "\t\t</Mark>\n".
        "\t\t<Size>6</Size>\n".
        "\t</Graphic>\n".
        "</PointSymbolizer>";


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputSample1XML(){

        //Well Known Name
        $wellKnownName = new WellKnownName(WellKnownName::SLD_CIRCLE);

        //Fill
        $style1 = CssParameter::fromValue(StyleParameter::NAME_FILL_COLOR, '#FF0000');
        $fill = new Fill(null, $style1);

        //Mark
        $mark = new Mark($wellKnownName, $fill);

        //Size
        $size = Size::fromValue(6);

        //Graphic
        $graphic = new Graphic(null, $mark, null, $size);

        //Point Symbolizer
        $ps = new PointSymbolizer($graphic);

        //Assert
        $this->assertSame(self::DATA_SAMPLE1, $ps->toXML());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputSample1XMLPrettified(){

        //Well Known Name
        $wellKnownName = new WellKnownName(WellKnownName::SLD_CIRCLE);

        //Fill
        $style1 = CssParameter::fromValue(StyleParameter::NAME_FILL_COLOR, '#FF0000');
        $fill = new Fill(null, $style1);

        //Mark
        $mark = new Mark($wellKnownName, $fill);

        //Size
        $size = Size::fromValue(6);

        //Graphic
        $graphic = new Graphic(null, $mark, null, $size);

        //Point Symbolizer
        $ps = new PointSymbolizer($graphic);

        //Assert
        $this->assertSame(self::DATA_SAMPLE1_PRETTIFIED, $ps->toXML(true));

    }


}
