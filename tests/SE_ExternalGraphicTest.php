<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/10/2018
 * Time: 3:44 PM
 */

use OGC\SLD\SE\ExternalGraphic;
use OGC\SLD\SE\Format;
use OGC\SLD\SE\OnlineResource;
use PHPUnit\Framework\TestCase;

class SE_ExternalGraphicTest extends TestCase
{

    const STANDARD_XML_PRETTIFIED =
        "<ExternalGraphic>\n".
        "\t<OnlineResource xlink:type=\"simple\" xlink:href=\"file:///var/www/htdocs/sun.png\" />\n".
        "</ExternalGraphic>";

    const STANDARD_XML =
        "<ExternalGraphic>".
        "<OnlineResource xlink:type=\"simple\" xlink:href=\"file:///var/www/htdocs/sun.png\" />".
        "</ExternalGraphic>";

    const FULL_XML_PRETTIFIED =
        "<ExternalGraphic>\n".
        "\t<OnlineResource xlink:type=\"simple\" xlink:href=\"file:///var/www/htdocs/sun.png\" />\n".
        "\t<Format>image/png</Format>\n".
        "</ExternalGraphic>";

    const FULL_XML =
        "<ExternalGraphic>".
        "<OnlineResource xlink:type=\"simple\" xlink:href=\"file:///var/www/htdocs/sun.png\" />".
        "<Format>image/png</Format>".
        "</ExternalGraphic>";


    /**
     * @test
     */
    public function testCanOutputStandardXML(){

        $resource = new OnlineResource('file:///var/www/htdocs/sun.png');
        $resource->applyNamespace(false);

        $externalGraphic = new ExternalGraphic($resource);

        $this->assertSame(self::STANDARD_XML, $externalGraphic->applyNamespace(false)->toXML());

    }

    /**
     * @test
     */
    public function testCanOutputStandardXMLPrettified(){

        $resource = new OnlineResource('file:///var/www/htdocs/sun.png');
        $resource->applyNamespace(false);

        $externalGraphic = new ExternalGraphic($resource);

        $this->assertSame(self::STANDARD_XML_PRETTIFIED, $externalGraphic->applyNamespace(false)->__toString());

    }


    /**
     * @test
     */
    public function testCanOutputFullXML(){

        $resource = new OnlineResource('file:///var/www/htdocs/sun.png');
        $resource->applyNamespace(false);

        $format = new Format(Format::FORMAT_PNG);
        $format->applyNamespace(false);

        $externalGraphic = new ExternalGraphic($resource, $format);
        $this->assertSame(self::FULL_XML, $externalGraphic->applyNamespace(false)->toXML());

    }

    /**
     * @test
     */
    public function testCanOutputFullXMLPrettified(){

        $resource = new OnlineResource('file:///var/www/htdocs/sun.png');
        $resource->applyNamespace(false);

        $format = new Format(Format::FORMAT_PNG);
        $format->applyNamespace(false);

        $externalGraphic = new ExternalGraphic($resource, $format);

        $this->assertSame(self::FULL_XML_PRETTIFIED, $externalGraphic->applyNamespace(false)->__toString());

    }

}
