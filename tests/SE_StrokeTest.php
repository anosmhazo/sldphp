<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/4/2018
 * Time: 9:04 AM
 */

use OGC\SLD\SE\Stroke;
use OGC\SLD\SE\StyleParameter;
use OGC\SLD\SE\SvgParameter;
use PHPUnit\Framework\TestCase;

class SE_StrokeTest extends TestCase
{

    const TEST_STANDARD_XML = "<Stroke></Stroke>";

    const TEST_STROKE_SVG_STYLE_XML_PRETTIFIED =
        "<Stroke>\n".
        "\t<SvgParameter name=\"stroke\">#0000FF</SvgParameter>\n".
        "\t<SvgParameter name=\"stroke-width\">3</SvgParameter>\n".
        "\t<SvgParameter name=\"stroke-dasharray\">5 2</SvgParameter>\n".
        "</Stroke>";

    const TEST_STROKE_SVG_STYLE_XML =
        "<Stroke>".
        "<SvgParameter name=\"stroke\">#0000FF</SvgParameter>".
        "<SvgParameter name=\"stroke-width\">3</SvgParameter>".
        "<SvgParameter name=\"stroke-dasharray\">5 2</SvgParameter>".
        "</Stroke>";

    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputStandardXML(){

        $parameter = Stroke::fromStyles();
        $this->assertSame(self::TEST_STANDARD_XML, $parameter->toXML());

    }

    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputSvgStylesXMLPrettified(){

        $style1 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_COLOR, '#0000FF');
        $style2 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_WIDTH, 3);
        $style3 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_DASH_ARRAY, '5 2');

        $parameter = Stroke::fromStyles($style1, $style2, $style3);
        $this->assertSame(self::TEST_STROKE_SVG_STYLE_XML_PRETTIFIED, $parameter->__toString());

    }

    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputSvgStylesXML(){

        $style1 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_COLOR, '#0000FF');
        $style2 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_WIDTH, 3);
        $style3 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_DASH_ARRAY, '5 2');

        $parameter = Stroke::fromStyles($style1, $style2, $style3);
        $this->assertSame(self::TEST_STROKE_SVG_STYLE_XML, $parameter->toXML());

    }

}
