<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/11/2018
 * Time: 1:55 PM
 */

use OGC\SLD\SE\Opacity;
use PHPUnit\Framework\TestCase;

class SE_OpacityTest extends TestCase
{

    const TEST_VALUE_XML = "<Opacity>3</Opacity>";

    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputValueXML(){

        $parameter = Opacity::fromValue(3);
        $this->assertSame(self::TEST_VALUE_XML, $parameter->toXML());

    }

}
