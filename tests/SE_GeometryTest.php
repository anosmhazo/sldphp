<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/3/2018
 * Time: 10:35 AM
 */

use OGC\SLD\FE\Expression;
use OGC\SLD\SE\Geometry;
use PHPUnit\Framework\TestCase;

class SE_GeometryTest extends TestCase
{

    const GEOMETRY_PROPERTY_XML =
        "<Geometry>".
        "<ogc:PropertyName>centerline</ogc:PropertyName>".
        "</Geometry>";

    const GEOMETRY_PROPERTY_XML_PRETTIFIED =
        "<Geometry>\n".
        "\t<ogc:PropertyName>centerline</ogc:PropertyName>\n".
        "</Geometry>";


    /**
     * @test
     */
    public function testCanOutputGeometryXML(){

        $expression = Expression::fromPropertyOnly('centerline');
        $geometry = new Geometry($expression);

        $this->assertSame(self::GEOMETRY_PROPERTY_XML, $geometry->toXML());

    }


    /**
     * @test
     */
    public function testCanOutputGeometryXMLPrettified(){

        $expression = Expression::fromPropertyOnly('centerline');
        $geometry = new Geometry($expression);

        $this->assertSame(self::GEOMETRY_PROPERTY_XML_PRETTIFIED, $geometry->toXML());

    }


}
