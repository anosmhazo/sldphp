<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/11/2018
 * Time: 1:55 PM
 */

use OGC\SLD\SE\Opacity;
use OGC\SLD\SE\Rotation;
use OGC\SLD\SE\Size;
use PHPUnit\Framework\TestCase;

class SE_RotationTest extends TestCase
{

    const TEST_VALUE_XML = "<Rotation>3</Rotation>";

    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputValueXML(){

        $parameter = Rotation::fromValue(3);
        $this->assertSame(self::TEST_VALUE_XML, $parameter->toXML());

    }

}
