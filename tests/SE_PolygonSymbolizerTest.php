<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/12/2018
 * Time: 10:46 AM
 */

use OGC\SLD\SE\CssParameter;
use OGC\SLD\SE\Fill;
use OGC\SLD\SE\Graphic;
use OGC\SLD\SE\Mark;
use OGC\SLD\SE\PolygonSymbolizer;
use OGC\SLD\SE\Size;
use OGC\SLD\SE\StyleParameter;
use OGC\SLD\SE\WellKnownName;
use PHPUnit\Framework\TestCase;

class SE_PolygonSymbolizerTest extends TestCase
{

    const DATA_SAMPLE1 =
        '<PolygonSymbolizer>'.
        '<Fill>'.
        '<CssParameter name="fill">#000080</CssParameter>'.
        '</Fill>'.
        '</PolygonSymbolizer>';

    const DATA_SAMPLE1_PRETTIFIED =
        "<PolygonSymbolizer>\n".
        "\t<Fill>\n".
        "\t\t<CssParameter name=\"fill\">#000080</CssParameter>\n".
        "\t</Fill>\n".
        "</PolygonSymbolizer>";

    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputSample1XML(){

        //Fill
        $style1 = CssParameter::fromValue(StyleParameter::NAME_FILL_COLOR, '#000080');
        $fill = new Fill(null, $style1);

        $ps = new PolygonSymbolizer(null, $fill);

        //Assert
        $this->assertSame(self::DATA_SAMPLE1, $ps->toXML());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputSample1XMLPrettified(){

        //Fill
        $style1 = CssParameter::fromValue(StyleParameter::NAME_FILL_COLOR, '#000080');
        $fill = new Fill(null, $style1);

        $ps = new PolygonSymbolizer(null, $fill);

        //Assert
        $this->assertSame(self::DATA_SAMPLE1_PRETTIFIED, $ps->toXML(true));

    }


}
