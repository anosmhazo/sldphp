<?php
/**
 * Created by PhpStorm.
 * User: Anos Kuda Mhazo
 * Date: 1/11/2018
 * Time: 11:19 AM
 */

use OGC\SLD\SE\WellKnownName;
use PHPUnit\Framework\TestCase;

class SE_WellKnownNameTest extends TestCase
{

    const STANDARD_XML = '<WellKnownName>circle</WellKnownName>';

    /**
     * @test
     */
    public function testCanOutputStandardXML(){

        $wellKnownName = new WellKnownName(WellKnownName::SLD_CIRCLE);
        $this->assertSame(self::STANDARD_XML, $wellKnownName->toXML());

    }
}
