<?php

/**
 * Rest SE Rule class
 * User: Anos Kuda Mhazo
 * Date: 12/28/2017
 * Time: 12:24 PM
 */

use OGC\SLD\FE\BinaryComparisonOperator;
use OGC\SLD\FE\Expression;
use OGC\SLD\SE\LineSymbolizer;
use OGC\SLD\SE\Rule;
use OGC\SLD\SE\RuleDescription;
use OGC\SLD\SE\Stroke;
use OGC\SLD\SE\StyleParameter;
use OGC\SLD\SE\SvgParameter;
use PHPUnit\Framework\TestCase;


class SE_RuleTest extends TestCase
{

    const STANDARD_XML = "<Rule></Rule>";
    const RULE_DESCRIPTION_XML = "<Rule><Description><Title>Sample Style</Title><Abstract>Sample Style Abstract</Abstract></Description></Rule>";

    const STANDARD_XML_PRETTIFIED = "<Rule>\n</Rule>";

    const RULE_DESCRIPTION_XML_PRETTIFIED =
        "<Rule>\n\t".
        "<Description>\n\t\t<Title>Sample Style</Title>\n\t\t<Abstract>Sample Style Abstract</Abstract>\n\t</Description>\n".
        "</Rule>";

    const RULE_DESCRIPTION_DENOMINATOR_XML_PRETTIFIED =
        "<Rule>\n".
        "\t<Description>\n".
        "\t\t<Title>Sample Style</Title>\n".
        "\t\t<Abstract>Sample Style Abstract</Abstract>\n".
        "\t</Description>\n".
        "\t<MinScaleDenominator>100000</MinScaleDenominator>\n".
        "\t<MaxScaleDenominator>1000000</MaxScaleDenominator>\n".
        "</Rule>";

    const RULE_DESCRIPTION_FILTER_XML_PRETTIFIED =
        "<Rule>\n".
        "\t<Description>\n".
        "\t\t<Title>Sample Style</Title>\n".
        "\t\t<Abstract>Sample Style Abstract</Abstract>\n".
        "\t</Description>\n".
        "\t<ogc:Filter>\n".
        "\t\t<ogc:PropertyIsGreaterThanOrEqualTo>\n".
        "\t\t\t<ogc:PropertyName>num_lanes</ogc:PropertyName>\n".
        "\t\t\t<ogc:Literal>4</ogc:Literal>\n".
        "\t\t</ogc:PropertyIsGreaterThanOrEqualTo>\n".
        "\t</ogc:Filter>\n".
        "</Rule>";

    const RULE_DESCRIPTION_FILTER_SYMBOLIZERS_XML_PRETTIFIED =
        "<Rule>\n".
        "\t<Description>\n".
        "\t\t<Title>Sample Style</Title>\n".
        "\t\t<Abstract>Sample Style Abstract</Abstract>\n".
        "\t</Description>\n".
        "\t<ogc:Filter>\n".
        "\t\t<ogc:PropertyIsGreaterThanOrEqualTo>\n".
        "\t\t\t<ogc:PropertyName>num_lanes</ogc:PropertyName>\n".
        "\t\t\t<ogc:Literal>4</ogc:Literal>\n".
        "\t\t</ogc:PropertyIsGreaterThanOrEqualTo>\n".
        "\t</ogc:Filter>\n".
        "\t<LineSymbolizer>\n".
        "\t\t<Stroke>\n".
        "\t\t\t<SvgParameter name=\"stroke\">#0000FF</SvgParameter>\n".
        "\t\t\t<SvgParameter name=\"stroke-width\">3</SvgParameter>\n".
        "\t\t\t<SvgParameter name=\"stroke-dasharray\">5 2</SvgParameter>\n".
        "\t\t</Stroke>\n".
        "\t\t<PerpendicularOffset>5</PerpendicularOffset>\n".
        "\t</LineSymbolizer>\n".
        "</Rule>";

    const RULE_DESCRIPTION_FILTER_SYMBOLIZERS_XML =
        "<Rule>".
        "<Description>".
        "<Title>Sample Style</Title>".
        "<Abstract>Sample Style Abstract</Abstract>".
        "</Description>".
        "<ogc:Filter>".
        "<ogc:PropertyIsGreaterThanOrEqualTo>".
        "<ogc:PropertyName>num_lanes</ogc:PropertyName>".
        "<ogc:Literal>4</ogc:Literal>".
        "</ogc:PropertyIsGreaterThanOrEqualTo>".
        "</ogc:Filter>".
        "<LineSymbolizer>".
        "<Stroke>".
        "<SvgParameter name=\"stroke\">#0000FF</SvgParameter>".
        "<SvgParameter name=\"stroke-width\">3</SvgParameter>".
        "<SvgParameter name=\"stroke-dasharray\">5 2</SvgParameter>".
        "</Stroke>".
        "<PerpendicularOffset>5</PerpendicularOffset>".
        "</LineSymbolizer>".
        "</Rule>";

    const TEST_NAME = 'Rule 1';
    const TEST_TITLE = 'Sample Style';
    const TEST_ABSTRACT = 'Sample Style Abstract';

    /**
     * @test
     */
    public function testCanOutputStandardXML(){

        $description = new Rule(null);
        $this->assertSame(self::STANDARD_XML, $description->toXML());

    }


    /**
     * @test
     */
    public function testCanOutputRuleWithDescriptionXML(){

        $description = new \OGC\SLD\SE\RuleDescription(self::TEST_TITLE,self::TEST_ABSTRACT);
        $rule = new Rule(null, $description);
        $this->assertSame(self::RULE_DESCRIPTION_XML, $rule->toXML());

    }


    /**
     * @test
     */
    public function testCanOutputStandardXMLPrettified(){

        $description = new Rule(null);
        $this->assertSame(self::STANDARD_XML_PRETTIFIED, $description->__toString());

    }


    /**
     * @test
     */
    public function testCanOutputRuleWithDescriptionXMLPrettified(){

        $description = new \OGC\SLD\SE\RuleDescription(self::TEST_TITLE,self::TEST_ABSTRACT);
        $rule = new Rule(null, $description);
        $this->assertSame(self::RULE_DESCRIPTION_XML_PRETTIFIED, $rule->__toString());

    }


    /**
     * @test
     */
    public function testCanOutputRuleWithDescriptionFilterXMLPrettified(){

        //Create description
        $description = new \OGC\SLD\SE\RuleDescription(self::TEST_TITLE,self::TEST_ABSTRACT);

        //Create expression + Create comparison operator + Create filter
        $expression = Expression::fromPropertyAndLiteral('num_lanes', 4);
        $comparator = new BinaryComparisonOperator(BinaryComparisonOperator::PROPERTY_IS_GREATER_THAN_OR_EQUAL_TO, $expression);
        $filter = new \OGC\SLD\FE\Filter($comparator);

        $rule = new Rule(null, $description, $filter);

        $this->assertSame(self::RULE_DESCRIPTION_FILTER_XML_PRETTIFIED, $rule->__toString());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputRuleWithDescriptionFilterSymbolizerXMLPrettified(){

        //Create description
        $description = new \OGC\SLD\SE\RuleDescription(self::TEST_TITLE,self::TEST_ABSTRACT);

        //Create expression + Create comparison operator + Create filter
        $expression = Expression::fromPropertyAndLiteral('num_lanes', 4);
        $comparator = new BinaryComparisonOperator(BinaryComparisonOperator::PROPERTY_IS_GREATER_THAN_OR_EQUAL_TO, $expression);
        $filter = new \OGC\SLD\FE\Filter($comparator);

        //Generate styling
        $style1 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_COLOR, '#0000FF');
        $style2 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_WIDTH, 3);
        $style3 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_DASH_ARRAY, '5 2');

        //Generate stroke
        $stroke = Stroke::fromStyles($style1, $style2, $style3);

        //Create symbolizer
        $symbolizer = new LineSymbolizer(null, $stroke);
        $symbolizer->setPerpendicularOffset(5);

        //Create rule
        $rule = new Rule(null, $description, $filter);
        $rule->addSymbolizers($symbolizer);

        $this->assertSame(self::RULE_DESCRIPTION_FILTER_SYMBOLIZERS_XML_PRETTIFIED, $rule->__toString());

    }


    /**
     * @test
     * @throws Exception
     */
    public function testCanOutputRuleWithDescriptionFilterSymbolizerXML(){

        //Create description
        $description = new \OGC\SLD\SE\RuleDescription(self::TEST_TITLE,self::TEST_ABSTRACT);

        //Create expression + Create comparison operator + Create filter
        $expression = Expression::fromPropertyAndLiteral('num_lanes', 4);
        $comparator = new BinaryComparisonOperator(BinaryComparisonOperator::PROPERTY_IS_GREATER_THAN_OR_EQUAL_TO, $expression);
        $filter = new \OGC\SLD\FE\Filter($comparator);

        //Generate styling
        $style1 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_COLOR, '#0000FF');
        $style2 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_WIDTH, 3);
        $style3 = SvgParameter::fromValue(StyleParameter::NAME_STROKE_DASH_ARRAY, '5 2');

        //Generate stroke
        $stroke = Stroke::fromStyles($style1, $style2, $style3);

        //Create symbolizer
        $symbolizer = new LineSymbolizer(null, $stroke);
        $symbolizer->setPerpendicularOffset(5);

        //Create rule
        $rule = new Rule(null, $description, $filter);
        $rule->addSymbolizers($symbolizer);

        $this->assertSame(self::RULE_DESCRIPTION_FILTER_SYMBOLIZERS_XML, $rule->toXML());

    }


    /**
     * @test
     */
    public function testCanOutputRuleWithDenominatorsXMLPrettified(){

        //Create description
        $description = new RuleDescription(self::TEST_TITLE,self::TEST_ABSTRACT);

        $rule = new Rule(null, $description);
        $rule->setMinScaleDenominator(100e3)->setMaxScaleDenominator(1e6);

        $this->assertSame(self::RULE_DESCRIPTION_DENOMINATOR_XML_PRETTIFIED, $rule->__toString());

    }

}